<?php
/**
 * @file DPMLog.inc The Import Queue Log plugin for drupal_set_message()
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('Logging using Drupal messages'),
  'class' => 'ImportQueue_Log_Backend_DrupalMessage',

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_EVENT
  )
);

/**
 * @class ImportQueue_Log_Backend_DrupalMessage
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_DrupalMessage extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message using drupal_set_message
   */
  public function log($message, Array $data, $severity) {
    if ($this->checkSeverity($severity)) {

      switch($severity) {
        case ImportQueue_Log::SEVERITY_CRITICAL:
        case ImportQueue_Log::SEVERITY_SEVERE:
        case ImportQueue_Log::SEVERITY_ERROR:
          $status = 'error';
          break;
        case ImportQueue_Log::SEVERITY_ISSUE:
        case ImportQueue_Log::SEVERITY_WARNING:
          $status = 'warning';
          break;
        default:
        case ImportQueue_Log::SEVERITY_EVENT:
        case ImportQueue_Log::SEVERITY_MESSAGE:
        case ImportQueue_Log::SEVERITY_DEBUG1:
        case ImportQueue_Log::SEVERITY_DEBUG2:
        case ImportQueue_Log::SEVERITY_DEBUG3:
        case ImportQueue_Log::SEVERITY_DEBUG4:
          $status = 'status';
      }

      drupal_set_message($message, $status);
    }
  }

}
