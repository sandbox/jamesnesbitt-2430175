<?php
/**
 * @file log.inc Import Queue logging utilities
 *
 * The Import Queue processes are intensive, and are
 * accessible through a variety of approaches, including
 * programmattically, Drush and through an admin UI
 *
 * Because there are so many different approches, it
 * makes sense to centralize the logging of events
 * and exceptions in a single place.
 *
 * @note if an exception is caught by the log object, that
 * is of a significant severity,  it will throw another
 * exception, in an attempt to stop execution.
 *
 * @TODO decide on an Exception error code strategy
 */

/**
 * Generate a log object, configured using the default/saved settings
 *
 * @param $type a short string key to the log settings array, which will
 *    give an array of saved setting matching that key.  If an array is passed
 *    then that array is used as log settings.
 *    It is preferred to use a string key, as the string key settings
 *    can be configured by administrators.
 *
 * @note the settings gfor the $type are configured by combining the
 *    defaults from the ctools plugin type importqueue_log, with the saved
 *    settings for the plugin type.  The saved settings can be changed
 *    by submitting the plugin type settings form.
 *
 * @returns an instance of ImportQueue_Log
 */
function importqueue_log_get($type='default') {
  $log =& drupal_static(__function__, array());

  if (is_array($type)) {
    return _importqueue_log_get($type);
  }

  if (empty($log[$type])) {
    $settings =& drupal_static(__function__.'_settings', array());
    if (empty($settings)) {
      $settings = importqueue_plugintype_settings_get('logbackend', _importqueue_log_get());
    }
    if (isset($settings[$type])) {
      // remove any plugins with 'active'=>FALSE
      foreach($settings[$type] as $plugin=>$plugin_settings) {
        if (!(isset($plugin_settings['active']) && $plugin_settings['active'])) {
          unset($settings[$type][$plugin]);
        }
      }
    }
    $log[$type] = _importqueue_log_get( isset($settings[$type]) ? $settings[$type] : array() );
  }
  return $log[$type];
}

/**
 * Create a standard instance of the ImportQueue_Log
 *
 * If no backends are specified, then use the NULL object
 * which does nothing really
 */
function _importqueue_log_get(Array $backends=NULL) {
  if (empty($backends)) {
    return new ImportQueue_Log_NULL(array());
  }
  else {
    return new ImportQueue_Log_standard($backends);
  }
}

/**
 * Form handler to configure tasks in general
 */
function importqueue_plugin_log_settings_form(&$form, &$form_state, $settings, $log) {
  importqueue_loadincludes();
  if (empty($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info('logbackend', NULL, $log);

  $form['instructions'] = array(
    '#markup' => '<p>Here you can configure the way the system will log events, under different circumstances.</p>
                  <p>For each logging case (default, drush, admin, debug) you can select which logging backend plugins will be active, and configure them.</p>'
  );

  foreach( array(
    'default'=>'Default Logger',
    'drush'=>'Drush Logger',
    'admin'=>'Admin interface Logger',
    'debug'=>'Debugging Logger',
  ) as $logbackend=>$label) {

    $form[$logbackend] = array(
      '#type' => 'fieldset',
      '#title' => t($label),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach($info['plugins'] as $key=>$plugin) {
      if (isset($info['abstract']) && $info['abstract']) {
        continue;
      }

      $class = $plugin['class'];
      $plugin_settings = drupal_array_merge_deep(
        isset($plugin['defaults']) ? $plugin['defaults'] : array(),
        isset($settings[$logbackend][$key]) ? $settings[$logbackend][$key] : array()
      );
      $active = isset($plugin_settings['active']);

      $form[$logbackend][$key] = array(
        '#type' => 'fieldset',
        '#title' => t($key),
        '#collapsible' => TRUE,
        '#collapsed' => !$active,

        'active' => array(
          '#type' => 'checkbox',
          '#title' => t('Active'),
          '#default_value' =>$active
        ),

      );
      $class::logger_form($form[$logbackend][$key], $form_state, $plugin_settings, $log);
    }

  }

  $form['severities'] = array(
    '#prefix' => '<h3>Severty interpretations</h3>',
    '#markup' => '
<p>
<br/> SEVERITY_CRITICAL=0;  <-- Stop all execution
<br/> SEVERITY_SEVERE=1;    <-- Stop any migration execution
<br/> SEVERITY_ERROR=2;     <-- an error has occurred, a migration step may be stopped but execution will continue
<br/>  // ----- ABOVE ITEMS WILL LIKELY PRODUCE A USER MESSAGE -----
<br/> SEVERITY_ISSUE=3;     <-- a noticable issue has arisen (minor error,) a migration step element (maybe a task loop, or item processing) may be skipped
<br/> SEVERITY_WARNING=4;   <-- something went wrong, but everything is OK (for example an item field or task field couldn\'t be processed)
<br/>  // ----- ABOVE ITEMS ARE CONSIDERED FAILURES TO SOME DEGREE ------
<br/> SEVERITY_EVENT=5;     <-- a significant event has occured such as the starting of a batch
<br/>  // ----- ABOVE ITEMS ARE GENERALLY INCLUDED IN LOGS -----
<br/> SEVERITY_MESSAGE=6;   <-- a step in a migration has occured, such as the processing of a task, or item
<br/>  // ----- BELOW ITEMS USUALLY DON\'T GET SHOWN TO ANYBODY EXCEPT DEVS -----
<br/> SEVERITY_DEBUG1=7;    <-- debugging details about elements that are being processed
<br/> SEVERITY_DEBUG2=8;    <-- ok, I can see where things went wrong
<br/> SEVERITY_DEBUG3=9;    <-- too much detail man, too much detail
<br/> SEVERITY_DEBUG4=10;   <-- Staaaaaaap
</p>
  '
  );
}

/**
 * @interface ImportQueue_Log
 *
 */
interface ImportQueue_Log {

  /**
   * Error severity codes
   *
   */
  const SEVERITY_CRITICAL=0;  // <-- Stop all execution
  const SEVERITY_SEVERE=1;    // <-- Stop any migration execution
  const SEVERITY_ERROR=2;     // <-- an error has occurred, a migration step may be stopped but execution will continue
  // ----- ABOVE ITEMS WILL LIKELY PRODUCE A USER MESSAGE -----
  const SEVERITY_ISSUE=3;     // <-- a noticable issue has arisen (minor error,) a migration step element (maybe a task loop, or item processing) may be skipped
  const SEVERITY_WARNING=4;   // <-- something went wrong, but everything is OK (for example an item field or task field couldn't be processed)
  // ----- ABOVE ITEMS ARE CONSIDERED FAILURES TO SOME DEGREE ------
  const SEVERITY_EVENT=5;     // <-- a significant event has occured such as the starting of a batch
  // ----- ABOVE ITEMS ARE GENERALLY INCLUDED IN LOGS -----
  const SEVERITY_MESSAGE=6;   // <-- a step in a migration has occured, such as the processing of a task, or item
  // ----- BELOW ITEMS USUALLY DON'T GET SHOWN TO ANYBODY EXCEPT DEVS -----
  const SEVERITY_DEBUG1=7;    // <-- debugging details about elements that are being processed
  const SEVERITY_DEBUG2=8;    // <-- ok, I can see where things went wrong
  const SEVERITY_DEBUG3=9;    // <-- too much detail man, too much detail
  const SEVERITY_DEBUG4=10;    // <-- Staaaaaaap

  /**
   * Constructor : create backends for log instance
   *
   * @param Array $options array of backends to use, key is plugin name, value is options array passed to plugin
   */
  public function __construct(Array $options);

  /**
   * Log a message
   *
   * @note this is pretty much just a wrapper for log(), with a default severity.
   *
   * @param String $message text
   * @param Integer $severity degree
   *
   * @return boolean success
   *
   * @see ImportQueue_Log SEVERITY list
   */
  public function message($message, Array $data=array(), $severity=ImportQueue_Log::SEVERITY_EVENT);

  /**
   * receive any exception, especially our ImportQueue_Exceptions
   *
   * @see ImportQueue_Exception
   * @see log()
   *
   * @param Exception $e exception to catch
   * @param Integer $severity of exeption (can be pulled from ImportQueue_Exceptions)
   *     @note if the exception has it's own severity value, then don't worry about this
   *
   * @returns boolean success
   *
   * @see ImportQueue_Log SEVERITY list
   */
  public function exception(Exception $e, $severity=ImportQueue_Log::SEVERITY_ISSUE);

}

/**
 * @class ImportQueue_Log_standard the normal log class
 *
 * @implements ImportQueue_log because that's it's purpose
 * @implements Serializable so that it doesn't get in the way if serialized (as it is used everywhere)
 */
class ImportQueue_Log_standard implements ImportQueue_Log, Serializable {

  /**
   * Keep a copy of the original options configuration (for serializing) and
   * an array of all active log backend plugins used to receive messages
   */
  private $options = array();
  private $backends = array();

  /**
   * Track the lowest severity of messages for the system
   *
   * @todo this is not used anywhere, it can be deprecated if it is not usefull
   */
  private $severity=self::SEVERITY_DEBUG4; // current severity, starts off lease severe (10)

  /**
   * Constructor : create backends for log instance
   *
   * @param Array $options array of backends to use, key is plugin name, value is options array passed to plugin
   */
  public function __construct(Array $options) {
    $this->options = $options;
    $this->build_backends();
  }

  /**
   * Small private function to build the backend plugin instances from settings Array
   *
   * @throws ImportQueue_ExceptionWarning if no log backend plugins can be found
   * @returns nothing
   */
  private function build_backends() {
    foreach($this->options as $backend=>$options) {
      $plugin = importqueue_plugin_get('logbackend', $backend, $options, $this);
      // is there no matching plugin then log that
      if (empty($plugin)) {
        // throw and catch a new exception for the missing backend
        try { throw new ImportQueue_ExceptionWarning('No known log backend plugin found "'.$backend.'", this plugin will be ignored.');}
        catch (Exception $e) { $this->exception($e); }
      }
      else {
        $this->backends[$backend] = $plugin;
      }
    }
  }

  /**
   * @note that this is not a perfect serialize, as log backends plugins
   *   are created again, meaning that if they track data, then that date is
   *   lost in the serializable process.
   */
  public function serialize() {
    return serialize($this->options);
  }
  public function unserialize($serialized) {
    $options = unserialize($serialized);
    $this->options = $options;
    $this->build_backends();
  }

  /**
  * Internally log a message by passing it to any included backends
  */
  private function log($message, Array $data=NULL, $severity=ImportQueue_Log::SEVERITY_MESSAGE) {
    if (!is_array($data)) {
      $data = array();
    }
    if (!is_int($severity)) {
      $severity=ImportQueue_Log::SEVERITY_MESSAGE;
    }
    if (!isset($data['time'])) {
      $data['time'] = microtime(true);
    }
    $data['message'] = $message;
    $data['severity'] = $severity;

    // track lowest severity (most severe)
    if ($severity<$this->severity) {
      $this->severity = $severity;
    }

    // iterate througgh the backends, and run each log method
    foreach($this->backends as $id=>$backend) {
      try {
        $backend->log($message, $data, $severity);
      } catch (Exception $e) {
        // just continue.  Try to get the rest of the log entries out
      }
    }

    /**
     * @NOTE if the severity of your passed exception was high enough, then another exception will
     *   be thrown.  This thrown exception will be of either ImportQueue_Log::SEVERITY_CRITICAL or SEVERE
     *   which can be caught inside a catch block and processed.
     *   It is described that CRITICAL means a full execution stop, while, SEVERE means halt the migration
     */
    if ($severity<=ImportQueue_Log::SEVERITY_CRITICAL) {
      throw new ImportQueue_ExceptionCritical('Import has processed a CRITICAL error; execution will stop', $data, 0, (isset($data['exception']) ? $data['exception'] : NULL));
    }
    else if ($severity<=ImportQueue_Log::SEVERITY_SEVERE) {
      throw new ImportQueue_ExceptionSevere('Import has processed a SEVERE error; migration will stop', $data, 0, (isset($data['exception']) ? $data['exception'] : NULL));
    }

  }

  /**
   * Public Logging methods
   *
   * These methods are publicly used to register log entries in the system
   * and can also be used to catch exceptions and interupt processing.
   */

  /**
   * Log a message
   *
   * @note this is pretty much just a wrapper for log(), with a default severity.
   *
   * @param String $message text
   * @param Integer $severity degree
   *
   * @return boolean success
   *
   * @see ImportQueue_Log SEVERITY list
   */
  public function message($message, Array $data=array(), $severity=ImportQueue_Log::SEVERITY_EVENT) {
    return $this->log($message, $data, $severity);
  }

  /**
   * receive any exception, especially our ImportQueue_Exceptions
   *
   * @see ImportQueue_Exception
   * @see log()
   *
   * @param Exception $e exception to catch
   * @param Integer $severity of exeption (can be pulled from ImportQueue_Exceptions)
   *     @note if the exception has it's own severity value, then don't worry about this
   *
   * @returns boolean success
   *
   * @see ImportQueue_Log SEVERITY list
   */
  public function exception(Exception $e, $severity=NULL) {

    /**
     * Interpret some stuff from exceptions
     * to pass to the backends as data
     */
    $data = array(
      'class' => get_class($e),
      'message' => $e->getMessage(),
      'file' => $e->getFile(),
      'line' => $e->getLine(),
//       'trace' =>$e->getTrace(),
      'trace' =>$e->getTraceAsString(),
      'exception'=>$e
    );

    // pull missing severtiy from exception if possible (Our Import Queue Exceptions know their own severity)
    if (!is_null($severity)) {
    }
    else if ($e instanceof ImportQueue_Exception) {
      $severity = $e->severity();
      $data += $e->getElements();
    }
    else {
      $severity = ImportQueue_Log::SEVERITY_ISSUE;
    }

    /**
     * This switch allows us to handle known exceptions differently
     * Any case that doesn't need a $this->log should return in it's
     * switch case, otherwise just break, and the log() will pass any
     * messages on to the logging backends.
     */
    switch (get_class($e)) {
      case 'ImportQueue_ExceptionCritical':
        $message = 'CRITICAL EXCEPTION: '.$data['message'].'['.$data['class'].']['.$data['file'].'::'.$data['line'].']';
        break;
      case 'ImportQueue_ExceptionSevere':
        $message = 'SEVERE EXCEPTION: '.$data['message'].'['.$data['class'].']['.$data['file'].'::'.$data['line'].']';
        break;
      case 'ImportQueue_ExceptionError':
        $message = 'EXCEPTION: '.$data['message'].'['.$data['class'].']['.$data['file'].'::'.$data['line'].']';
        break;

      default:
        $message = $data['message'].'['.$data['class'].']['.$data['file'].'::'.$data['line'].']';
        $elements = array();
    }

    $return = $this->log($message, $data, $severity);

    return $return;
  }

}

/**
 * @class ImportQueue_Log_NULL An no-action log replacement for use during bootstrap
 *
 * @note this is used in cases where we can't safely create an import log object, but
 *   one is required for a method arguments.  We use this if the creation of a log
 *   object would create an infinite loop, or if it failed to be created.
 *
 * @implements ImportQueue_Log
 */
class ImportQueue_Log_NULL implements ImportQueue_Log {

  public function __construct(Array $options) { }

  public function message($message, Array $data=array(), $severity=ImportQueue_Log::SEVERITY_EVENT) { }
  public function exception(Exception $e, $severity=ImportQueue_Log::SEVERITY_ISSUE) { }

}

/**
 * @interface ImportQueue_LogBackend The base requirements from a Import Queue logging backend
 *
 * Classes that implement this class, are considered appropriate logging handlers.
 * Instances of the classes will be created to handled the various log messages, through
 * execution of the log() method in the Logging Handler
 *
 * @see ImportQueue_Log
 */
interface ImportQueue_Log_Backend {

  /**
   * @function Log a message in the Import Queue System
   *
   * @param $message for the log entry
   * @param $severity integer from 1-10
   * @return a boolean, indicating success
   */
  public function log($message, Array $data, $severity);

}

/**
 * @class ImportQueue_Log_Backend_Base Optional parent log backend plugin
 *
 * This is an optional parent Log Backend plugin, which has some boilerplate code
 * for handling severity and construction.
 *
 * @see ImportQueue_Log_Backend
 */
abstract class ImportQueue_Log_Backend_Base extends ImportQueue_Plugin_Base implements ImportQueue_Plugin, ImportQueue_Log_Backend {

  /**
   * @attribute ImportQueue_Log object
   *
   * @note that it is dangerous to play too much with this object in this
   *    context as you can easily create infinite loops by logging in the log()
   *    method
   */
  protected $log;

  /**
   * @attribute MinimumSeverity
   *
   * Don't respond to log items of less significane (higher number)
   * than this value
   */
   protected $MinimumSeverity=5;

  /**
   * Constructor : just implement the interface requirements.
   *
   * @note this backend needs no options
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    parent::__construct($options, $log);

    if (isset($this->options['MinimumSeverity'])) {
      $this->MinimumSeverity = (int) $this->options['MinimumSeverity'];
    }
  }

  /**
   * Severity Checker
   */
  protected function checkSeverity($severity) {
    return ($this->MinimumSeverity >= (int)$severity);
  }

  /**
   * Log the message using dpm
   */
  abstract public function log($message, Array $data, $severity);

  /**
   * Show a configuration form, based on passed in settings
   */

  static public function logger_form(&$form, &$form_state, Array $settings, ImportQueue_Log $log) {

    $form['MinimumSeverity'] = array(
      '#type' => 'textfield',
      '#title' => t('Severity'),
      '#size' => 5,
      '#description' => t('Any messages received with larger severity index than this number will be ignored'),
      '#default_value' => isset($settings['MinimumSeverity']) ? $settings['MinimumSeverity'] : ImportQueue_Log::SEVERITY_ERROR
    );

  }

}

/**
 * @interface ImportQueue_Exeption Base Excepton interface that is aware of it's own severity
 *
 */
interface ImportQueue_Exception {

  /**
   * Report an integer severity of the Exception
   *
   * @see ImportQueue_Log severity
   * @returns and integer severity, 1-10, lower is more severe
   */
  public function severity();

  /**
   * Retrieve an array of mixed data, that pertain to
   * the exception, which could be used for logging
   */
  public function getElements();

}

/**
 * @class ImportQueue_ExceptionBase Abstract Base Import Queue Exception class
 *
 * Generic Import Queue Exception handler
 *
 * @see Execeptions
 * @implements ImportQueue_Exception
 */
abstract class ImportQueue_ExceptionBase extends Exception implements ImportQueue_Exception {

  // a default severity of "issue"
  static protected $severity=ImportQueue_Log::SEVERITY_ISSUE;

  // keep any elements marked for the exeption
  protected $elements=array();

  // hand back the static severity value
  public function severity() {
    return static::$severity;
  }

  public function __construct($message="", $elements=array(), $code=0, Exception $previous=NULL) {
    $this->elements = (array)$elements;
    parent::__construct($message, $code, $previous);
  }

  public function getElements() {
    return $this->elements;
  }

}

/**
 * Message level exception : Nothing serious
 */
class ImportQueue_ExceptionMessage extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_MESSAGE;
}

/**
 * Warning level exception : relatively insignificant error
 */
class ImportQueue_ExceptionWarning extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_WARNING;
}

/**
 * Issue level exception : Error, some element step will be skipped (a task row, or an item field) but migration process (task/item) can continue
 */
class ImportQueue_ExceptionIssue extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_ISSUE;
}

/**
 * Error level exception : Significant Error, migration element may be skipped (task/item) but migration will continue
 */
class ImportQueue_ExceptionError extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_ERROR;
}

/**
 * Sever Error level exception : Sever Error, import should stop, but execution can continue
 */
class ImportQueue_ExceptionSevere extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_SEVERE;
}

/**
 * Critical Error level exception : Serious error, execution should be aborted
 */
class ImportQueue_ExceptionCritical extends ImportQueue_ExceptionBase {
  static protected $severity=ImportQueue_Log::SEVERITY_CRITICAL;
}
