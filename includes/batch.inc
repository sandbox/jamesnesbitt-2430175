<?php
/**
 * @file batch.inc Centralize all batching abstraction here.
 *
 * This file contains a number of helper functions, which concentrate
 * the batch processing into a single include.
 * @todo this code could be refactored to put the item/task related functions
 * into the related includes.
 *
 * Use this code to process your batches:
 * 1. Use the importqueue_X_batch() method to retrieve a batch configuration
 *    array for your batch type.
 * 2. Override anything in the array to meet your needs, such as changing
 *    the 'finished' function.
 * 3. Run the _importqueue_batch_process() function, passing in your batch
 *    array, and a redirect URL if you are not running the batch from a form.
 *
 */

/**
 * Process a BatchAPI batch, setting it and calling process if necessary
 *
 * @param Array $batch array to be passed to batch_set()
 * @param String $redirect URL. Leave this empty if executed from a form
 *
 * @return nothing
 */
function _importqueue_batch_process($batch, $redirect = ''){
  batch_set($batch);
  if(!empty($redirect)){
    batch_process($redirect);
  }
}


/**
 * TASKS BATCHING
 */

/**
 * Generate a batch array for running tasks
 *
 * @param Array of batches, each value is a task plugin name
 *   There are two formats for the tasks:
 *   1. The array value is a string key for a plugin, in which case, an empty options
 *      array will be passed to the plugin when it is created.
 *   2. The key is the string key for a plugin, and the value is an options array which
 *      will be passed into the plugin when it is constructed.
 * @param Array/String $logtype for which log settings should be used for batch logging.  This
 *   could also be an array of backend configurations.
 *
 * @returns Array that can be passed into batch_set()
 */
function importqueue_tasks_batch(Array $tasks=NULL, $logtype='default') {
  importqueue_loadincludes('batch','log');
  $log = importqueue_log_get($logtype);

  try {

    if (empty($tasks)) {
      $settings = importqueue_plugintype_settings_get('importqueue_task', NULL, $log);
      $tasks = (array)$settings['default_tasks_batch'];
    }

    $batch = array(
      'operations' => array(),
      'title' => t('Import Tasks Batch'),
      'init_message' => t('Import tasks batch is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Import Task Batch has encountered an error.'),
      'finished' => 'importqueue_batch_tasks_finish',
      'file' => drupal_get_path('module', 'importqueue') . '/includes/batch.inc',
    );
    foreach($tasks as $task=>$options) {
      if (!is_array($options)) {
        $task = $options;
        $options = array();
      }

      /**
       * This just makes using this function with checkboxes easier, as checkboxes
       * in Drupal FormAPI submit 0 values for unchecked boxes.
       */
      if (empty($task)) {
        continue;
      }

      $batch['operations'][] = array(
        'importqueue_task_batch_handler',
        array($task, $options, $logtype)
      );
    }

    return $batch;

  } catch (Exception $e) {
    $this->log->exception($e);
  }

}

/**
 * batchAPI handler for running a task as a batch operation
 *
 * This function should get called by the BatchAPI to run a batched processing
 * of a single task plugin.  The function basically hands off execution to
 * the task plugin run method, through the importqueue_task_run() function
 * which is found in includes/task.inc
 * @note This function, and the plugin->run() method will be run repeatedly,
 *   until the plugin itself sets $context['finished'] => 1
 *
 * @param String $task plugin name
 * @param Array of options to pass to the plugin constructor
 * @param String $logtype identifier, matching one of the importqueue_logbackend
 *     settings keys.  This could also be an array of log backend configurations
 *     @see importqueue_log_get()
 * @param Array batchAPI $context array, which is passed to the task plugin.
 *
 * @returns nothing.  Alter the $context array to affect workflow.
 *
 * @see BatchAPI
 */
function importqueue_task_batch_handler($task, $options, $logType, &$context) {
  try {
    importqueue_loadincludes('batch','task');
    $log = importqueue_log_get($logType);

    if (isset($context['sandbox']['pass'])) {
      $context['sandbox']['pass']++;
    }
    else {
      $context['sandbox']['pass'] = 1;
    }

    $context['message'] = 'Running Task batch ['.$task.'] pass #'.$context['sandbox']['pass'];
    importqueue_task_run($task, $options, $context, $log);
  } catch (Exception $e) {
    /**
      * @note the log object will be given exceptions if any occur. That object will
      *    decide if the exception is serious enough to stop batch execution. If it
      *    wants to stop execution, it will throw it's own exception here.
      */
    $log->exception(new ImportQueue_ExceptionError('An exception occured during execution of a migration batch.  The batch will be aborted.', array('context'=>$context,'task'=>$task, 'options'=>$options), 0 , $e));
  }
}
function importqueue_batch_tasks_finish($success, $results, $operations) {
  // do thing, override this function

}

/**
 * ITEMS BATCHING
 */

/**
 * Produce a BatchAPI array for batch processing a number of Queue items
 *
 * @param Integer $limit on homw many items to process.
 * @param Array $filters array, passed to the queue_get() function for queue item retrieval.
 * @param String $logtype which key of log settings should be used to create the log object used.
 *
 * @return $batch array, which can be passed to batch_set()
 *
 * @see BatchAPI
 */
function importqueue_items_batch($limit, Array $filters = NULL, $logtype = 'default') {
  importqueue_loadincludes('item');

  $batch = array(
    'operations' => array(),
    'title' => t('Import Queue Batch'),
    'init_message' => t('Import items batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Import Queue Batch has encountered an error.'),
    'finished' => 'importqueue_batch_items_finish',
    'file'          =>  drupal_get_path('module', 'importqueue') . '/includes/' . 'batch.inc',
  );

  $limit = (int)$limit;

  /**
   * @note originally we created one batch operation for each item in the
   *    limit, but we recently switched to using a single operation with the
   *    limit passed in, which allows us to halt the batch, if the queue is out
   *    of items, preventing the overhead of the batch function processing
   *    empty queue items.
   */
  $batch['operations'][] = array(
    'importqueue_batch_items_handler',
    array($filters, $limit, $logtype)
  );

  return $batch;
}

/**
 * batchAPI Handler for processing the next item from the system.
 *
 * @param Array $filters array passed to the queue handler to retrieve queue items.
 * @param Integer $limit how many items to process (maximum) in this batch.
 * @param String/Array configuration index or array for log handling.
 * @param Array $context BatchAPI context.
 *
 * @return nothing.  BatchAPI implementations alter the $context array to control workflow.
 */
function importqueue_batch_items_handler($filters, $limit, $logType, &$context) {
  try {
    importqueue_loadincludes('log', 'queue', 'item');
    $log = importqueue_log_get($logType);

    if (!isset($context['sandbox']['pass'])) {
      $context['sandbox']['pass'] = 0;
      $context['finished'] = 0;
      if (empty($limit)) {
        $context['sandbox']['limit'] = 0;
      }
      else {
        $context['sandbox']['limit'] = $limit;
      }
    }
    $context['sandbox']['pass']++;

    // retrieve the next item from the queue, that matches our filters
    $item = importqueue_queue_item_get($filters);

    if ($item instanceof ImportQueue_Queue) {
      $context['message'] = 'Running Item batch ['.$item->data()->id.'] pass #'.$context['sandbox']['pass'];
      importqueue_item_run($item, $log, $context);
      if (!empty($limit)) {
        $context['finished'] = $context['sandbox']['pass'] / $limit;
      }
    }
    else {
      $context['finished'] = 1;
      $context['message'] = 'Processed all items in queue, pass #'.$context['sandbox']['pass'];
    }

  } catch (Exception $e) {
    /**
      * @note the log object will be given exceptions if any occur. That object will
      *    decide if the exception is serious enough to stop batch execution. If it
      *    wants to stop execution, it will throw it's own exception here.
      */
    $log->exception($e);
  }
}
function importqueue_batch_items_finish($success, $results, $operations) {
  // this is for functional elements post batch, as opposed to UI elements
}
