<?php
/**
 * @file DBQueue.inc The Import Queue Queue handler for DB Table based queues
 *
 * This is a custom replacement for the DrupalQueue based handler, which can properly
 * implement the features that were missing from the DrupalQueue approach:
 *   - propery filtering without having to byPass most of the DrupalQueue functionality
 *   - retrieve information on an item without claiming it (for admin purposes)
 *   - keep data on an item after it is closed.
 */

/**
 * @class ImportQueue_Queue_DB
 *
 * @implements ImportQueue_Plugin        : this is a plugin
 * @implements ImportQueue_Queue         : this plugin is a queue handler
 */
class ImportQueue_Queue_DB extends ImportQueue_Plugin_Base implements ImportQueue_Plugin, ImportQueue_Queue, ImportQueue_Log_SelfLogger {

  /**
   * Queue Item Data , and Queue Item specific Informations
   */
  protected $item_status = importqueue_Queue::STATUS_QUEUED;
  protected $item_data = NULL;
  protected $item_info = array();

  /**
   * Initializer : set up the plugin class
   */
  static public function __init(Array $info, Array $options, ImportQueue_Log $log) {
    parent::__init($info, $options, $log);

    /**
     * Here we make sure that we have the tables that we need
     */
    if (!db_table_exists('importqueue_queue')) {
      foreach (static::schema() as $name => $table) {
        db_create_table($name, $table);
      }
    }

  }

  /**
   * Constructer : set up the class instance
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    parent::__construct($options, $log);

    if (isset($this->options['info'])) { $this->item_info = $this->options['info']; }
    if (isset($this->options['status'])) { $this->item_status = $this->options['status']; }

    if (!isset($this->options['data'])) {
      throw new importqueue_QueueException_QueueItemHasNoData('Tried to create a queue item with no data', array('options'=>$this->options));
    }

    $this->item_data = $this->options['data'];
  }

  /**
   * Log a message in the Import Queue System
   *
   * @param $message for the log entry
   * @param $severity integer from 1-10
   * @return a boolean, indicating success
   *
   * @todo expand this if we want to use a log approach to allow.
   *    reversible operations, as opposed to keeping data in the
   *    item metadata.  We could keep all sorts of stuff here
   *
   * @see ImportQueue_Log_SelfLogger
   */
  public function log($message, Array $data, $severity) {
    $index =& drupal_static(__method__, 0);
    $info =& $this->info();
    if (!isset($info['log'])) { $info['log'] = array(); }
    while (isset($info['log'][REQUEST_TIME.'_'.sprintf('%03d', $index)])) { $index++; }
    $info['log'][REQUEST_TIME.'_'.sprintf('%03d', $index)] = $message;
    $this->save();
  }

  /**
   * Add a new ItemData object to the queue
   *+
   * @param ImportQueue_Queue_ItemData $item struct
   * @return boolean success
   *
   * @see ImportQueue_Queue_DB_Information for the queueinfo
   */
  static public function createItem(ImportQueue_DataStruct $data, $status=ImportQueue_Queue_Item::STATUS_QUEUED, ImportQueue_Log $log) {
    $info = array();
    // this may be silly, we add any non-array/object meta values to the info, so that they show up as top level values in the .info file. It is for readabi
    foreach($data->meta() as $key=>$value) {
      switch(FALSE) {
        case is_array($value):
        case is_object($value):
          break;
        default:
          $info[$key] = $value;
      }
    }

    $info['created'] = REQUEST_TIME;
    $info['priority'] = 5; // CURRENTLY THIS DOESN'T DO ANYTHING

    $info['log'] = array(REQUEST_TIME.'_0'=>'Created new queue item ['.__method__.'::'.__line__.']');

    $class = __CLASS__;
    $item = new $class(array('info'=>$info, 'data'=>$data, 'status'=>$status), $log);
    if ($item->save()) {
      return $item;
    }
    else {
      throw new ImportQueue_ExceptionError('['.__class__.'] Could not create a queue item', array('item'=>$item));
    }
  }

  /**
   * Get the next queue item, that matches optional filter criteria
   *
   * @return ImportQueue_Queue_Item instance, which can release() and close()
   *     Or empty/NULL if no queue items found
   */
  static public function getItem(Array $metaFilters=NULL, ImportQueue_Log $log) {
    $items = static::filterItems($metaFilters, 1, $log);
    if (count($items)>0) {
      $item = reset($items);
      $item->claim();
      return $item;
    }
  }
  /**
   * List the queue items that match the passed filter, for admin use
   *
   * @todo improve the filtering methodology, maybe allow multiple values?
   *
   * @note this functionality was implemented before by pulling all
   *   of the items from the queue, and then releasing them.  This is
   *   a better way, that may allow us to check on items in other
   *   states
   *
   * @return an array of ImportQueue_DataStruct objects for queue items
   */
  static public function listItems(Array $metaFilters=NULL, ImportQueue_Log $log) {
    if (isset($metaFilters['limit'])) {
      $limit = $metaFilters['limit'];
      unset($metaFilters['limit']);
    }
    else {
      $limit = 0;
    }

    return  importqueue_plugin_get('tool', 'DataStruct', array('meta'=>array('filters'=>$metaFilters), 'data'=>static::filterItems($metaFilters, $limit, $log)), $log);
  }

  /**
   * Return the status
   */
  public function status() {
    return $this->item_status;
  }
  /**
   * Return the Queue information
   */
  public function info() {
    return $this->item_info;
  }
  /**
   * Return the item data
   */
  public function data() {
    return $this->item_data;
  }

  /**
   * mark an item claimed
   *
   * @return boolean success
   */
  public function claim() {
    if ($this->status()!=$this::STATUS_CLAIMED) {
      $this->item_status = $this::STATUS_CLAIMED;
      return $this::saveItem($this, 'Item claimed', $this->log);
    }
    return $this->status()==$this::STATUS_CLAIMED;
  }

  /**
   * Release a retrieved item from processing, leaving it unprocessed
   *
   * @return boolean success
   */
  public function release() {
    if ($this->status()==$this::STATUS_CLAIMED) {
      $this->item_status = $this::STATUS_QUEUED;
      return $this::saveItem($this, 'Item released', $this->log);
    }
    return $this->status()==$this::STATUS_QUEUED;
  }

  /**
   * Close a retrieved item marking it processed
   *
   * @return boolean success
   */
  public function close() {
    if ($this->status()!=$this::STATUS_CLOSED) {
      $this->item_status = $this::STATUS_CLOSED;
      return $this::saveItem($this, 'Item closed', $this->log);
    }
    return $this->status()==$this::STATUS_CLOSED;
  }

  /**
   * FAIL a retrieved item marking it not-processed
   *
   * @return boolean success
   */
  public function fail() {
    if ($this->status()!=$this::STATUS_FAILED) {
      $this->item_status = $this::STATUS_FAILED;
      return $this::saveItem($this, 'Item failed', $this->log);
    }
    return $this->status()==$this::STATUS_FAILED;
  }

  /**
   * Remove an item from the system
   *
   * @return boolean success
   */
  public function remove() {
    return FALSE;
  }

  /**
   * Save changes to an item
   *
   * @return boolean success
   */
  public function save() {
    return static::saveItem($this, 'Item save', $this->log);
  }

  /**
   * Admin settings form for the plugin
   *
   * IF this class were to implement ImportQueue_Configurable, then this function would
   * be used to present a settings form in the ImportQueue admin uI
   *
   * @see ImportQueue_Configurable
   */
  static public function options_form(Array &$form, &$form_state, ImportQueue_Log $log) {
    $options = static::$plugin_options['path'];

    /**
     * @todo put form elements here if you want this to be ImportQueue_Configurable
     *
     * @note just put form elements here, and they will be merged into static::$plugin_options and $this->options
     */

  }

  /**
   * Filter items
   */
  static private function filterItems(Array $filters, $limit=0, ImportQueue_Log $log) {
    if (isset($filters['id'])) {
      $filters['uuid'] = $filters['id'];
      unset($filters['id']);
    }

    $query = db_select('importqueue_queue', 'q')
      ->fields('q');

    foreach($filters as $key=>$filter) {
      if (!in_array($key, array('qid','batch','created','modified','uuid','priority','type','action','status'))) {
        continue;
      }

      if (!is_array($filter)) { $filter = array('value'=>$filter); }

      if (!isset($filter['op'])) { $filter['op'] = '='; }
      if (!isset($filter['value'])) { continue; }

      $query->condition('q.'.$key, $filter['value'], $filter['op']);
    }
    if ($limit>0) {
      $query->range(0, $limit);
    }

    $query_results = $query->execute();
    $results = array();
    $class = __CLASS__;
    while($result = $query_results->fetchAssoc()) {
      $info = $result;

      $info['log'] = unserialize($result['log']);

      $data = unserialize($result['data']);
      unset($info['data']);

      $results[] = new $class(array('info'=>$info, 'data'=>$data, 'status'=>$result['status']), $log);
    }
    return $results;
  }

  /**
   * Save an item
   */
  static private function saveItem(ImportQueue_Queue $item, $message, ImportQueue_Log $log) {
    $info = $item->info(); // this is an array

    if (!is_array($info['log'])) { $info['log'] = array(); }
    $log_index=0;
    while (isset($info['log'][REQUEST_TIME.'_'.$log_index])) { $log_index++; }
    $info['log'][REQUEST_TIME.'_'.$log_index] = $message;

    if (isset($info['qid'])) {
      return db_update('importqueue_queue')
        ->fields(array(
          'status' => $item->status(),
          'modified' => REQUEST_TIME,
          'data'=> serialize($item->data()),
          'log' => serialize($info['log'])
        ))
        ->condition('qid', $info['qid'], '=')
        ->execute();
    }
    else {
      $data = $item->data(); // this is an ImportQueue_DataStruct
      $info['qid'] = db_insert('importqueue_queue')
        ->fields(array(
          'status' => $item->status(),
          'type' => $data->type,
          'action' => $data->action,
          'uuid' => $data->id,
          'priority' => $info['priority'],
          'created' => REQUEST_TIME,
          'data'=> serialize($data),
          'log' => serialize($info['log'])
        ))
        ->execute();
      return $info['qid'];
    }
  }

  /**
   * Remove item
   */
  static private function removeItem(ImportQueue_Queue $item, ImportQueue_Log $log) {
    $info = $item->info();

    return db_delete('importqueue_queue')
      ->condition('qid', $info['qid'], '=')
      ->execute();
  }

  /**
   * DB Schemas
   *
   * Fields:
   *  qid : unique queue identifier
   *  data : serialized data for a queue item
   *  log : some kind of serialized log data, probably just an array
   *
   * @note All of the other fields are essentially used just for queue item filtering.
   * @note when this was written, batch id was not being populated, but the scheme needed
   *   to be somewhat future-proof.
   */
  static public function schema() {
    return array(
      'importqueue_queue' => array(
        'description' => 'ImportQueue DB Queue: Queue table',
        'fields' => array(
          'qid' => array(
            'description' => 'Primary Key: Sequential Queue item id.',
            'type' => 'serial',
            'unsigned' => TRUE,
            'not null' => TRUE,
          ),
          'batch' => array(
            'description' => 'Unique batch identifier, if this item was created using a batch.',
            'type' => 'varchar',
            'length' => 64,
            'not null' => FALSE,
            'default' => '',
          ),
          'status' => array(
            'description' => 'Queue item status, one of the ImportQueue_Queue::STATUS_X values',
            'type' => 'varchar',
            'length' => 255,
            'not null' => TRUE,
            'default' => '',
          ),
          'created' => array(
            'description' => 'A Unix timestamp indicating when the queue item was created.',
            'type' => 'int',
            'not null' => TRUE,
            'default' => 0,
          ),
          'modified' => array(
            'description' => 'A Unix timestamp indicating when the queue item was last modified.',
            'type' => 'int',
            'not null' => FALSE,
            'default' => 0,
          ),
          'priority' => array(
            'description' => 'The integer priority of the Queue Item.  THIS IS CURRENTLY NOT REALLY USED.',
            'type' => 'int',
            'not null' => FALSE,
            'default' => 0,
          ),
          'type' => array(
            'description' => 'Queue Item type (defines which handler will process the queue item.)',
            'type' => 'varchar',
            'length' => 32,
            'not null' => TRUE,
            'default' => '',
          ),
          'action' => array(
            'description' => 'Queue item action, which gives the item handler some more information about what to do with the item.',
            'type' => 'varchar',
            'length' => 255,
            'not null' => FALSE,
            'default' => '',
          ),
          'uuid' => array(
            'description' => 'UUID unique identifier for the queue item, used to uniquely retrieve a specific item, and to identify if a queue item already exists.',
            'type' => 'varchar',
            'length' => 255,
            'not null' => TRUE,
            'default' => '',
          ),
          'data' => array(
            'description' => 'Contents of the Queue Item.  This should actually be a serialized ImportQueue_DataStruct',
            'type' => 'blob',
            'not null' => TRUE,
            'size' => 'big',
          ),
          'log' => array(
            'description' => 'The Queue Item log, an array of messages showing the history of the Queue Item.',
            'type' => 'blob',
            'not null' => FALSE,
            'size' => 'big',
          ),
        ),
        'primary key' => array('qid'),
      )
    );
  }

}
