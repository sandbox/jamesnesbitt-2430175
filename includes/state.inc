<?php
/**
 * @file state.inc State based functionlity
 *
 * Some plugins will need to have some kind of tracking on state of the
 * plugin overall.  This is called state.
 *
 * Currently any class/plugin can mark itself as stateful by implementing the
 * ImportQueue_Stateful interface.  This indicates that a class will provide a
 * state implementation.
 *
 * ImportQueue_Plugins that want state access can base themselves off of the
 * ImportQueue_plugin_BaseWState class instead of the ImportQueue_plugin_Base
 * class.
 *
 */

/**
 * @interface ImportQueue_Stateful object maintains a state
 */
interface ImportQueue_Stateful {

  /**
   * Save the current state, with changes made
   *
   * @todo move the state array to be a self-saving object, so we don't need to save
   */
  public function state_save();

  /**
   * Empty the current state, with changes made
   */
  public function state_reset();

  /**
   * Provide a Drupal form for displaying/editing the state
   */
  static public function state_form(Array &$form, &$form_state, Array $state, ImportQueue_Log $log);

}

/**
 * @class ImportQueue_plugin_BaseWState A Base plugin that implements state
 *
 * @extends ImportQueue_plugin_Base
 * @implements @ImportQueue_State
 */
class ImportQueue_plugin_BaseWState extends ImportQueue_plugin_Base implements ImportQueue_Stateful {

  /**
   * Save an accessable copy of the state
   */
  protected $state = array();

  /**
   * Plugin constructor, run parent, but also retrieve state
   *
   * @param Array $options instance options overrides
   * @param ImportQueue_Log $log Import Queue logging object
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    parent::__construct($options, $log); // saves the $info array which we use later on
    $this->state = importqueue_plugin_state_get(static::$info['type'], static::$info['id'], $this->log);
  }

  /**
   * Save the current state, with changes made
   */
  public function state_save() {
    return importqueue_plugin_state_set(static::$info['type'], static::$info['id'], $this->state, $this->log);
  }

  /**
   * Reset the current state, with changes made
   */
  public function state_reset() {
    $this->state = array();
    return importqueue_plugin_state_set(static::$info['type'], static::$info['id'], array(), $this->log);
  }

  /**
   * Admin State form handler for the plugin
   *
   * @param $form drupal form array for just this admin form (not the whole form)
   * @param $form_state drupal form handler form state array
   *
   * @return hmmm
   */
  static public function state_form(Array &$form, &$form_state, Array $options, ImportQueue_Log $log) {
    // By default have no administration form

    $form['no state'] = array(
      '#markup' => '<strong>This plugin has no state</strong>'
    );

    return $form;
  }

}


/**
 * Retrieve state for a plugin type
 */
function importqueue_plugintype_state_get($type, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $variable = 'importqueue_state_'.$type;
  $state = variable_get($variable, array());
  $log->message('Retrieving plugin type state for : '.$type , array('type'=>$type, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG3);
  return $state;
}
/**
 * Save state for a plugin type
 */
function importqueue_plugintype_state_set($type, Array $state, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, NULL, $log);
  $variable = 'queuebackend_state_'.$type;
  if (empty($state)) {
    $log->message('Reetting plugin type state for : '.$type , array('type'=>$type, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_del($variable);
  }
  else {
    $log->message('Saving plugin type state for : '.$type , array('type'=>$type, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_set($variable, $state);
  }
}

/**
 * Retrieve plugin state
 */
function importqueue_plugin_state_get($type, $plugin, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, $plugin, $log);
  $module = $info['module'];
  $variable = $module.'_state_'.$type.'_'.$plugin;

  $state = variable_get($variable, array());
  $log->message('Retrieving plugin state for : '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG3);
  return $state;
}
/**
 * Save plugin state
 */
function importqueue_plugin_state_set($type, $plugin, Array $state, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, $plugin, $log);
  $module = $info['module'];
  $variable = $module.'_state_'.$type.'_'.$plugin;
  if (empty($state)) {
    $log->message('Resetting plugin state for : '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_set($variable, $state);
  }
  else {
    $log->message('Saving plugin state for : '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'state'=>$state), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_set($variable, $state);
  }
}
