<?php
/**
 * @file tool.inc Base abtstraction for tool plugins
 *
 * Tool plugins are base plugins used by other plugins to provide
 * Specialized functionality that may be shared across other plugins.
 *
 * Tools conform to the base plugin standard, but have no
 * additional requirements.
 * It is common to include the tool via a plugin retrieve/build,
 * but you can use tool classes directly after including them by
 * retrieving the plugin info.
 *
 */

/**
 * @interface ImportQueue_Tool
 *
 * @note Currently there are not interface details
 */
interface ImportQueue_Tool {

}
