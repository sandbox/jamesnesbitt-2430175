<?php
/**
 * @file DPMLog.inc The Import Queue Log plugin for Creating interface reports
 *
 * @see ImportQueue_Log_Backend
 *
 * This plugin works by keeping the reports inside a static list, which
 * can be accessed by a static method on the class
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('A report generating logger for manual output'),
  'class' => 'ImportQueue_Log_Backend_report',

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_MESSAGE,
    'DrupalMessageOnDestruct' => '',
    'DDOnDestruct' => '',
  )
);

/**
 * @class ImportQueue_Log_Backend_dpm
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_report extends ImportQueue_Log_Backend_Base {

  // keep all log entries, across all instances
  static protected $messages = array();

  // Only capture messages of this severity or more (lower number)
  protected $MinimumSeverity = ImportQueue_Log::SEVERITY_EVENT;

  // configure whether or not to output a report on close
  protected $DrupalMessageOnDestruct = FALSE;
  protected $DDOnDestruct = FALSE;

  /**
   * Constructor : just implement the interface requirements.
   *
   * @param Array $options array(
   *                    'DrupalMessageOnDestruct' => '{type}' : DPM a report on class __destruct
   *                    'DDOnDestruct' => '{type}' : DrupalDebug a report on class __destruct
   *                )
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    parent::__construct($options, $log);

    $this->DrupalMessageOnDestruct = isset($this->options['DrupalMessageOnDestruct']) ? $this->options['DrupalMessageOnDestruct'] : $this->DrupalMessageOnDestruct;
    $this->DDOnDestruct = isset($this->options['DDOnDestruct']) ? $this->options['DDOnDestruct'] : $this->DDOnDestruct;

  }

  /**
   * If directed to do so, output a report when the object dies
   *
   * @TODO this doesn't actually work  :(  fix it or take it out
   */
  public function __destruct() {
    if ($this->DrupalMessageOnDestruct) {
      drupal_set_message(static::report($this->DrupalMessageOnDestruct));
    }
    if ($this->DDOnDestruct && module_exists('devel')) {
      dd(self::report($this->DDOnDestruct), __method__);
    }
  }

  /**
   * Log the message using dpm
   */
  public function log($message, Array $data, $severity) {
    if ($this->checkSeverity($severity)) {

      // add some details to the array, so we can simplify keeping log entries
      $data['message'] = $message;
      $data['severity'] = $severity;

      self::$messages[] = $data;

    }
  }

  /**
   * Collect a report of items
   */
  static public function report($output, Array $filters=array()) {
    $items = self::$messages; // make a safe copy of the log

    // filter items
    foreach ($filters as $type=>$filter) {
      switch($type) {
        case 'severity':
          if (!is_array($filter)) { $filter = array('value'=>$filter); }
          $filter += array(
            'op' => '<',
            'value' => 5,
          );
          foreach($items as $index=>$item) {
            switch($filter['op']) {
              case '=':
                if ($item['severity'] != $filter['value']) { unset($items[$index]); }
                break;
              case '>':
                if ($item['severity'] <= $filter['value']) { unset($items[$index]); }
                break;
              case '>=':
                if ($item['severity'] < $filter['value']) { unset($items[$index]); }
                break;
              case '<':
                if ($item['severity'] >= $filter['value']) { unset($items[$index]); }
                break;
              default:
              case '<=':
                if ($item['severity'] > $filter['value']) { unset($items[$index]); }
                break;
            }
          }
      }
    }

    // output type
    switch($output) {
      default:
      case 'table':
        return self::table($items);
    }
  }

  /**
   * output log entries as an html table
   *
   * @todo this is pretty crude, and rather verbose.  It needs a more intuitive implementation
   */
  static protected function table($items) {
    $output = array(
      '#theme' => 'table',
      '#attributes' => array('class' => array('importqueue_report_table')),
      '#caption' => t('Import Queue Report'),
      '#header' => array(
        array('data'=>'time'),
        array('data'=>'severity'),
        array('data'=>'message'),
        array('data'=>'data'),
      ),
      '#rows' => array()
    );

    foreach($items as $item) {
      $row = array(
        array('data'=>$item['time']),
        array('data'=>$item['severity']),
        array('data'=>$item['message'])
      );

      unset($item['time'], $item['severity'], $item['message']);

      $data = '';
      foreach($item as $key=>$value) {
        if (is_object($value)) {
          $value = '|'.get_class($value).'|';
        }
        else if (is_array($value)) {
          $value = var_export($value,TRUE);
        }
        else {
          $value = nl2br( (string)$value );
        }

        $data .= '<dt>'.$key.'</dt><dd>'.var_export($value,TRUE).'</dd>';
        //$data .= '<dt>'.$key.'</dt><dd></dd>';
      }
      $row[] = array('data'=>'<dl>'.$data.'</dl>');
      //$row[] = array('data'=>var_export($item, true));

      $output['#rows'][] = $row;
    }
    return $output;
  }


  static public function logger_form(&$form, &$form_state, Array $settings, ImportQueue_Log $log) {

    parent::logger_form($form, $form_state, $settings, $log);

    $reports = array(
      '' => t('None'),
      'table' => t('HTML Log table')
    );

    $form['DrupalMessageOnDestruct'] = array(
      '#type' => 'select',
      '#title' => t('Output report to Drupal_message on Destruct'),
      '#options' => $reports,
      '#default_value' =>$settings['DrupalMessageOnDestruct'],
    );
    $form['DDOnDestruct'] = array(
      '#type' => 'select',
      '#title' => t('Output Report to DD on Destruct'),
      '#options' => $reports,
      '#default_value' =>$settings['DDOnDestruct'],
    );

  }

}
