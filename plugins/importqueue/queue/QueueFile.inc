<?php
/**
 * @file FileQueue.inc The Import Queue Queue handler for file based queues
 *
 * This is a complex queue system, that uses a folder full of files and subfolders
 * to manage the Import Queue Queue.
 *
 * File Structure:
 *   root/
 *     queue.info <-- INFO file used to track queue metadata
 *     /queued/  <-- Items in the queue
 *     /claimed/ <-- Items claimed, but not closed
 *     /closed/  <-- Closed items
 *
 *     /{status}/ <-- any queue status really, so new queue states should work
 *
 * A queue item is kept as a .item file inside one of the three folders, as
 * matches it's current state.  The file is moved between the folders to mark
 * the state.
 * This approach keeps a readable, accessible queue list, that could even by
 * manipulated by an external system if desired.
 *
 * This plugin is nice because it creates an easy to use file based environment
 * which can be navigated easily, and it can also handle completely arbitrary
 * filters.
 *
 * @bug duplicate checking is very expensive in this plugin.
 * @bug if there are multiple items
 *
 * @note this plugin will likely be deprecated soon.
 */

/**
 * @class ImportQueue_Queue_FileQueue
 *
 * @implements ImportQueue_Plugin         : this is a plugin
 * @implements ImportQueue_Configurable  : this plugin is configurable
 * @implements ImportQueue_Queue          : this plugin is a queue handler
 */
class ImportQueue_Queue_FileQueue extends ImportQueue_Plugin_Base implements ImportQueue_Plugin, ImportQueue_Configurable, ImportQueue_Queue, ImportQueue_Log_SelfLogger {

  /**
   * File extenstion for queue item files
   */
  const ITEM_FILE_EXTENSION = 'item';

  /**
   * Default path to queue items
   */
  static private $path = 'public://importqueue/';

  // Queue metadata object - holds some data such as the latest queue item index
  static protected $queueInfo = NULL;

  /**
   * Queue Item Data , and Queue Item specific Informations
   */
  protected $item_status = importqueue_Queue::STATUS_QUEUED;
  protected $item_data = NULL;
  protected $item_info = array();

  /**
   * Initialzer
   */
  static public function __init(Array $info, Array $options, ImportQueue_Log $log) {
    parent::__init($info, $options, $log);

    /**
     * @note do we want to be able to change static::$path, and have that change saved, if so use &$options['path']
     */
    static::$path = isset(static::$plugin_options['path']) ? static::$plugin_options['path'] : static::$path;

    /**
     * try to use the QueueInfo tool to generate new queue item data
     */
    static::$queueInfo = new ImportQueue_Queue_FileQueue_Information(static::$path);

    try {
      /**
       * @todo this makes lot's of PHP error exceptions for bad paths.  Is there a way to clean this up.  TRY doesn't catch the errors
       */
      if (!file_prepare_directory(static::$path, FILE_CREATE_DIRECTORY & FILE_MODIFY_PERMISSIONS)) {
        throw new ImportQueue_QueueException_CannotInitializeQueue('Import Queue file based queue system could not gain to the required queue path ['.static::$path.'] . Consider '.l('configuring', 'admin/config/vub/import/plugin/importqueue_queuebackend/QueueFile').' this plugin to use a different path.', array());
      }

    } catch (Exception $e) {
      $log->exception($e);
      throw new ImportQueue_QueueException_CannotInitializeQueue('File Based Queue handler failed to initialize', array('options'=>static::$plugin_options), 0, $e);
    }
  }

  /**
   * constructor : create a queue item instance
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    parent::__construct($options, $log);

    if (isset($this->options['info'])) { $this->item_info = $this->options['info']; }
    if (isset($this->options['status'])) { $this->item_status = $this->options['status']; }

    if (!isset($this->options['data'])) {
      throw new importqueue_QueueException_QueueItemHasNoData('Tried to create a queue item with no data', array('options'=>$this->options));
    }

    $this->item_data = $this->options['data'];
  }

  /**
   * Add a new ItemData object to the queue
   *
   * @param ImportQueue_QueueData $item struct
   * @param String a importqueue_Queue::STATUS_{status} value
   * @param ImportQueue_Log a logging object
   *
   * @return boolean success
   *
   * @see ImportQueue_Queue_FileQueue_Information for the queueinfo
   */
  static public function createItem(ImportQueue_DataStruct $data, $status=ImportQueue_Queue::STATUS_QUEUED, ImportQueue_Log $log) {
    $info = array();
    // this may be silly, we add any non-array/object meta values to the info, so that they show up as top level values in the .info file. It is for readabi
    foreach($data->meta() as $key=>$value) {
      switch(FALSE) {
        case is_array($value):
        case is_object($value):
          break;
        default:
          $info[$key] = $value;
      }
    }
    $info['created'] = REQUEST_TIME;
    $info['log'] = array(REQUEST_TIME.'_0'=>'Created new queue item ['.__method__.'::'.__line__.']');

    $class = __CLASS__;
    $item = new $class(array('info'=>$info, 'data'=>$data, 'status'=>$status), $log);
    return $item->save();
  }

  /**
   * Get the next queue item, that matches optional filter criteria
   *
   * @return ImportQueue_Queue instance, which can release() and close()
   *     Or empty/NULL if no queue items found
   */
  static public function getItem(Array $metaFilters=NULL, ImportQueue_Log $log) {
    $items = static::filterSubPathFiles(static::STATUS_QUEUED, $metaFilters, 1, $log);
    if (count($items)>0) {
      $item = reset($items);
      $item->claim();
      return $item;
    }
  }
  /**
   * List the queue items that match the passed filter, for admin use
   *
   * @todo improve the filtering methodology, maybe allow multiple values?
   *
   * @note this functionality was implemented before by pulling all
   *   of the items from the queue, and then releasing them.  This is
   *   a better way, that may allow us to check on items in other
   *   states
   *
   * @bug this function fails to find an item if there are duplicates using the same id
   *   which is the weakness of this plugin.
   *
   * @return an array of ImportQueue_DataStruct objects for queue items
   */
  static public function listItems(Array $metaFilters=NULL, ImportQueue_Log $log) {
    if (isset($metaFilters['limit'])) {
      $limit = $metaFilters['limit'];
      unset( $metaFilters['limit'] );
    }
    else {
      $limit = 0;
    }

    $statuses = array();
    if (isset($metaFilters['status'])) {
      $statuses[] = $metaFilters['status'];
      unset($metaFilters['status']);
    }
    else {
      $statuses = array(
        ImportQueue_Queue::STATUS_CLAIMED,
        ImportQueue_Queue::STATUS_FAILED,
        ImportQueue_Queue::STATUS_QUEUED,
        ImportQueue_Queue::STATUS_CLOSED,
      );
    }

    $items = array();
    foreach($statuses as $status) {
      $items += static::filterSubPathFiles($status, $metaFilters, $limit, $log);
    }
    return  importqueue_plugin_get('tool', 'DataStruct', array('meta'=>array('statuses'=>$statuses, 'filters'=>$metaFilters, 'limit'=>$limit), 'data'=>$items), $log);
  }

  /**
   * Return the status
   */
  public function status() {
    return $this->item_status;
  }
  /**
   * Return the Queue information
   */
  public function info() {
    return $this->item_info;
  }
  /**
   * Return the item data
   */
  public function data() {
    return $this->item_data;
  }

  /**
   * mark this item claimed
   *
   * @return boolean success
   */
  public function claim() {
    return $this->changeStatus($this::STATUS_CLAIMED, 'Item claimed');
  }

  /**
   * Release a retrieved item from processing, leaving it unprocessed
   *
   * @return boolean success
   */
  public function release() {
    return $this->changeStatus($this::STATUS_QUEUED, 'Item release/requeued');
  }

  /**
   * Close a retrieved item marking it processed
   *
   * @return boolean success
   */
  public function close() {
    return $this->changeStatus($this::STATUS_CLOSED, 'Item closed');
  }

  /**
   * Mark the item as processed and closed in the queue
   */
  public function fail() {
    return $this->changeStatus($this::STATUS_FAILED, 'Item failed');
  }


  /**
   * Change status on an item
   */
  private function changeStatus($newStatus, $message) {
    switch ($newStatus) {
      case ImportQueue_Queue::STATUS_QUEUED:
      case ImportQueue_Queue::STATUS_CLAIMED:
      case ImportQueue_Queue::STATUS_CLOSED:
      case ImportQueue_Queue::STATUS_FAILED:
        if ($this->status()!=$newStatus) {
          if ( $this::file_saveItem($this, $newStatus, $message, $this->log) ) {
            $this->item_status = $newStatus;
          }
        }
        return $this->status()==$newStatus;
      default:
      throw new ImportQueue_ExceptionError('Instruction to change item state referred to an invalid state :'.$newStatus, array('item'=>$item, 'status'=>$newStatus));
    }
  }

  /**
   * Mark the item as processed and closed in the queue
   */
  public function remove() {
    return $this::file_removeItem($this, $this->log);
  }

  /**
   * Save changes to an item
   *
   * @return boolean success
   */
  public function save() {
    return $this::file_saveItem($this, $this->status(), 'Saving item', $this->log);
  }

  /**
   * Admin settings form for the plugin
   *
   * @see ImportQueue_Configurable
   */
  static public function options_form(Array &$form, &$form_state, ImportQueue_Log $log) {
    $options = static::$plugin_options;

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('File Queue root path'),
      '#description' => t('What root path should be used to contain all of the Queue files.<br/><strong>Note</strong> that changing this path most likely means losing all existing queue items.'),
      '#default_value' => isset($options['path']) ? $options['path'] : '',
    );

  }

  /**
   * Log a message in the Import Queue System
   *
   * @param $message for the log entry
   * @param $severity integer from 1-10
   * @return a boolean, indicating success
   *
   * @todo expand this if we want to use a log approach to allow.
   *    reversible operations, as opposed to keeping data in the
   *    item metadata.  We could keep all sorts of stuff here
   *
   * @see ImportQueue_Log_SelfLogger
   */
  public function log($message, Array $data, $severity) {
    $index =& drupal_static(__method__, 0);
    $info =& $this->info();
    if (!isset($info['log'])) { $info['log'] = array(); }
    while (isset($info['log'][REQUEST_TIME.'_'.sprintf('%03d', $index)])) { $index++; }
    $info['log'][REQUEST_TIME.'_'.sprintf('%03d', $index)] = $message;
    $this->save();
  }

  /**
   * Save a Queue Item to a queue file
   *
   * This function is heavily documented because it has gone through a number of revisions
   * and needed a lot of clarity on what it is doing.
   *
   * @param ImportQueue_Queue A queue item (it should be a ImportQueue_Queue_QueueFile item)
   * @param String Optional new status
   * @param String Optional log message
   * @param ImportQueue_Log logging object
   * @returns boolean success
   *
   * @throws Exception if any exceptions occur that are considered severe
   */
  static private function file_saveItem(ImportQueue_Queue $item, $newStatus=NULL, $message, ImportQueue_Log $log) {
    try {
      // retrieve any existing file queue data
      $info = $item->info();

      // initialize an info array if it is empty
      if (empty($info)) {
        $info['created'] = REQUEST_TIME;
        $info['log'] = array();
      }
      else {
        $info['modified'] = REQUEST_TIME;
      }

      /**
      * Determine what queue subpath we are dealing with.  If the item already
      * exists, and it was using a different subpath then move the old queue file
      * to the new subpath.
      */
      $status = $item->status();
      if (!empty($newStatus) && $status!=$newStatus) {
        if (isset($info['filename']) && file_exists(static::$path.'/'.$status.'/'.$info['filename'])) {
          /**
            * @todo should we just use a file_unmanaged_delete() ?  there is only a small risk that
            *    a delete succeeds here, but a save later fails, which would make the file go missing
            */
          file_unmanaged_move( static::$path.'/'.$status.'/'.$info['filename'], static::$path.'/'.$newStatus.'/'.$info['filename'] );
        }
        // override the old status with our new status
        $status = $newStatus;
      }
      // generate a new subpath to be used
      $info['subpath'] = '/'.$status;

      /**
      * Pick a filename for the Queue item, use the previous one if one exists
      */
      if (!isset($info['filename'])) {
        $info['filename'] = static::item_newFileName($item);
      }

      /**
        * We had some problem generating file details
        * for this item, so we should probable fail here
        */
      if (empty($info['subpath']) || empty($info['filename'])) {
        throw new ImportQueue_ExceptionError('Could not generate queue item file name or path', array('item'=>$item, 'queue_info'=>$info));
      }

      // keep a copy of the full path of the queue item file
      $info['path'] = static::$path.$info['subpath'].'/'.$info['filename'];

      $path = static::$path.$info['subpath'];
      $filename = $info['filename'];

      /**
        * Make sure that we have a proper directory to pu the
        * Queue File.  Throw and exception if we can't
        */
      if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY & FILE_MODIFY_PERMISSIONS)) {
        throw new ImportQueue_ExceptionError('Could not generate queue subpath for status :'.$path, array('item'=>$item, 'queue_info'=>$info));
      }

      // Add the item meta and data to the info array
      /**
      * @todo Find a way to directly 'jsonify' the DataStruct so that we don't rely directly
      *    on ->meta() and ->data(), which is poor coupling (to deep and strict.)
      *    it would be better if we could serialize the DataStruct directly, but when I
      *    tried to do so, I found that when writing the serialized data to a file, it was
      *    truncating the data to around 500 characters or so.
      */
      $info['item'] = array(
        'meta' => $item->data()->meta(),
        'data' => $item->data()->data()
      );

      /**
      * remove the subpath and filename, so that we don't write it into the file.
      * it would be superfluous to have the data in a file, and would be incorrect
      * if the file was manually moved or moved outside of this handler.
      */
      unset($info['subpath'], $info['filename'], $info['path']);

      /**
      * It is in the interpretation of the item file that there has been
      * lots of effort, and more effort is required.
      * The ultimate item file would be interpretable, but also be human
      * readable, and editable.
      * The current implementation is straight JSON.
      */
      return file_unmanaged_save_data(drupal_json_encode($info), $path.'/'.$filename, FILE_EXISTS_REPLACE);

    } catch (Exception $e) {
      $log->exception($e);
      return FALSE;
    }
  }
  /**
   * Create a queue item from a queue item file
   *
   * @param String status that matches one of the three ImportQueue_Item::STATE_*
   * @param String filename for the queue file
   *
   * @return ImportQueue_Queue_QueueFile Item
   *
   * @throws Exception if any exceptions occur that are considered severe
   */
  static private function file_readItem($status, $filename, ImportQueue_Log $log) {
    $subpath = '/'.$status;

    /**
     * It is in the interpretation of the item file that there has been
     * lots of effort, and more effort is required.
     * The ultimate item file would be interpretable, but also be human
     * readable, and editable.
     * The current implementation is straight JSON.
     */

    // get interpreted JSON data from the file
    $info = file_get_contents(static::$path.$subpath.'/'.$filename);
    $info = drupal_json_decode($info);

    if (empty($info)) {
      throw new ImportQueue_ExceptionError('Queue item appears to be empty : '.$subpath.'/'.$filename, array('queue_info'=>$info));
    }
    if (!(is_array($info) && isset($info['item']))) {
      throw new ImportQueue_ExceptionError('Queue item file did not return the required item data', array('queue_info'=>$info));
    }

    try {

      /**
       * Here we keep some metadata about the Queue Item, in the item itself,
       * which will be used if/when that item is saved, so that we can keep
       * track of changes etc.
       */
      $info['status'] = $status;
      $info['filename'] = $filename;
      $info['subpath'] = $subpath;

      $data = importqueue_plugin_get('tool', 'DataStruct', $info['item'], $log);
      unset($info['item']); // this element is only used to build the struct
      $class = __CLASS__;
      $item = new $class(array('info'=>$info, 'data'=>$data, 'status'=>$status), $log);

      return $item;

    } catch (Exception $e) {
      $log->exception($e);
    }
  }

  /**
   * Remove an item from the Queue by removing the file item.
   */
  static protected function file_removeItem(ImportQueue_Queue $item, $log) {
    $info = $item->info();

    $subpath = isset($info['subpath']) ? $info['subpath'] : '/'.$item->status();
    $path = static::$path.$subpath.'/'.$info['filename'];

    if (file_unmanaged_delete($path)) {
      return TRUE;
    }
    else {
      throw new importqueue_QueueException_CannotProcessQueueItem('Queue item file could not be delete.', array('item'=>$item, 'path'=>$path));
    }
  }

  /**
   * generate a name for a new item file
   *
   * @todo use a hexbased number instead of decimal, to get more files out
   *    of the index var space ( PHP::dechex() )
   * @todo include item priority in the filename, so that priority items automatically
   *    come first.
   *
   * @param ImportQueue_QueueData $item struct
   *
   * @return boolean success
   *
   * @see ImportQueue_Queue_FileQueue_Information for the queueinfo
   */
  static protected function item_newFileName($item) {
    if (empty(static::$queueInfo)) {
      return false;
    }

    $fileIndex = (int)static::$queueInfo->currentIndex + 1; // a missing value will become 0 due to type casting
    static::$queueInfo->currentIndex = $fileIndex; // update the index with the new index value

    $filename = sprintf('%09d', $fileIndex);
    $filename = substr($filename,0,32).'.'.STATIC::ITEM_FILE_EXTENSION;

    return $filename;
  }

  /**
   * A utility method that scans for matching items in a queue folder
   *
   * In this QueueHandler, we search for a matching queue item, by looping
   * through all of the queue items in the subfolder, until a file
   * with matching metadata is found.  Each file is loaded, and searched,
   * before moving to the next file.  The order of the files is most
   * likely alphabetic, and the files are named using an incremental
   * value, so the oldest item should be checked first.
   *
   * @param String $status for items, allowing us to pick a single sub-folder.
   * @param Array Filter array, holding a meta key=>value pair, that items must match.
   * @param Integer Optional limit to the number of items to retrieve.
   *
   * @returns array of matching ImportQueue_Queue_QueueFile objects.
   *
   * @throws Exception if any exceptions occur that are considered severe
   */
  static private function filterSubPathFiles($status, Array $metaFilters=NULL, $limit=0, $log) {
    $items = array();
    foreach(file_scan_directory(static::$path.'/'.$status, '/.*\.'.static::ITEM_FILE_EXTENSION.'$/') as $filename=>$file) {
      try {
        $item = static::file_readItem($status, $file->filename, $log);

        if (!($item instanceof ImportQueue_Queue)) {
          throw new ImportQueue_ExceptionError('Queue File read operation failed to return a queue item.', array('plugin'=>__CLASS__,'options'=>self::$plugin_options));
          continue;
        }

        $data = $item->data();
        // process each of the filters
        foreach((array)$metaFilters as $field=>$filter) {
          if (!is_array($filter)) {
            $filter = array('value'=>$filter);
          }
          $filter += array(
            'op' => '=',
          );
          $op = $filter['op'];
          $value = $data->$field;
          switch (true) {
            case ($op=='=' && $filter['value']!=$value):
            case ($op=='!=' && $filter['value']==$value):
            case ($op=='<' && $filter['value']>=$value):
            case ($op=='<=' && $filter['value']>$value):
            case ($op=='>' && $filter['value']<=$value):
            case ($op=='>=' && $filter['value']<$value):
              continue 3;
          }
        }

        $items[$file->filename] = $item;
      } catch (Exception $e) {
        $log->exception($e);
        continue;
      }

      // if a limit was passed, make sure that we haven't reached it
      if ($limit>0 && count($items)>=$limit) {
        break;
      }
    }
    return $items;
  }

}

/**
 * @class ImportQueue_Queue_FileQueue_Information  as small utility class to keep a keyed json metadata file in a path
 *
 * This object is a path based keyed data file, that keeps a local data
 * which is saved back to the source file on change
 */
class ImportQueue_Queue_FileQueue_Information {

  protected $path='';
  protected $data=array();

  public function __construct($path) {
    $this->path = $path.'/queue.info';
    $this->data = (array)static::readFile($this->path);
  }

  public function __set($key, $value) {
    $this->data[$key] = $value;
    return static::writeFile($this->path, $this->data);
  }
  public function __get($key) {
    if (isset($this->data[$key])) {
      return $this->data[$key];
    }
  }

  static protected function readFile($path) {
    if (file_exists($path)) {
      try {
        $data = file_get_contents($path);
        $data = drupal_json_decode($data);
      } catch (Exception $e) {
        throw new ImportQueue_QueueException_CannotInitializeQueue('Could not read data from the FileQueue meta information file : '.$path, array('path'=>$path), 0, $e);
      }
    }
    else {
      $data = array();
      static::writeFile($path,array());
      if (!file_exists($path)) {
        throw new ImportQueue_QueueException_CannotInitializeQueue('Could not create the FileQueue to hold queue meta information file : '.$path, array('path'=>$path), 0, $e);
      }
    }
    return $data;
  }
  static protected function writeFile($path, $data) {
    if (!file_unmanaged_save_data(drupal_json_encode($data), $path, FILE_EXISTS_REPLACE)) {
      throw new ImportQueue_QueueException_CannotInitializeQueue('Could not write to the FileQueue meta information file : '.$path);
    }
  }

}
