- THE IMPORTQUEUE MODULE -

This module is the new migration procedure, offering an abstracted and modular
approach to migrations from backend sources.

== TODOS ==

- Implement queue item priority
- Implement queue item dependencies
- Make state self-saving
- Make state instance based/allow multiple states per plugin

== THE CONCEPT ==

The modular nature of the module is intended to allow developers to write
small pieces of code in ctools plugins to define to the system new remote
locations/sources of data, and to define new local entities that can be
populated from those sources.  The process of determining which data needs
to be migrated, and actually migrating those items is split, allowing
optimization of processing and scheduling for each to be controlled.

=== RUNNING IMPORTS ===

Imports are run in pieces, with each piece being executable independently.
There are outer general functions that are used to run the pieces as
batches in pieces.  There are Drush methods that tie into these general
functions.

To run a batch to process tasks:
@TODO importqueue_tasks_batch()
@TODO drush vit
@TODO goto /admin/importqueue/tasks

To run a batch to process items:
importqueue_items_batch()
@TODO drush vii
@TODO goto /admin/importqueue/items

=== THE QUEUE ===

The queue is a central mechanism that goes holds a FIFO list of migration
items that need to be processed.  It will hold an item type, and some serialized
data to be passed to the appropriate handler.

The queue is implemented through a number of interfaces, primarily centered
around the queue backend plugin.  This allows different queue backend
implementations to be used.  Note that switching backends does not copy
existing queue items between the backends.
There are currently two backends written:
1. DrupalQueue : a backend written that hands off queue items to DrupalQueue
2. FileQueue : a file based queue that creates JSON files per queue item,
and moves them between different folders to track state.

When creating a queue item, you need only pass in 3 things:
1. the type of item plugin that will be used to handle the item
2. an array of arbitrary metadata for the item, used typically in filtering
3. an array of data for the item, meant to provide an item plugin
with all it needs to process the item.

When retrieving a queue item, you will receive an instance of the
ImportQueue_Queue_Item interace, from which you can ->data() to get
an instance of the ImportQueue_DataStruct interface.
The latter gives you access to all of the MetaData (including type)
and item data, while the former has methods you can use to mark the
queue item as processed or to return it back to the queue as
unprocessed (->close() and ->release() )

To create a queue item:
includes/queue.inc::importqueue_queue_item_add( ... )

To retrieve a queue item:
includes/queue.inc::importqueue_queue_item_get( ... )

=== TASK PLUGINS ===

Task plugins are expected to be atomic operations that contact a backend
source, and add items to the central queue.
Task plugins have ->run() methods which are meant to be Drupal BatchAPI
aware, which populate the import queue internally.

=== ITEM PLUGINS ===

Item plugins are expected to be single item processors that can act on
a single queue item, of a matching type, and create/update any number of
related system entities.
Item plugins receive a queue item, and are expected to process that
item, and then mark the item as processed or failed (released)
Item plugins are typically run in batches, just like task plugins,
however they tend to do all of their work in a single pass.

== THE LOG ==

The logging system is a manner for passing messages and errors around.
The center of the system are various instances of the ImportQueue_Log class,
which each write to their own collection of backend plugins.
The backend plugins are unique instances, that may share some backend data
depending on implementation.  For example, the watchdog based plugin passes
all messages through the drupal_watchdog function, which is centralized.

There is a high verbosity HTML Report backend, which keeps all messages
and let's you output them into an HTML table for debugging/display.

To get an instance of the default log object use: importqueue_log_get()
To get an instance of a debugging log object use: importqueue_log_get('debug')

To get an instance of a custom logger, just for a specific usecase or debugging,
create your own instance using _importqueue_log_get().

To use the log object, either run the ->log( ... ) method, or pass it an
exeptions using the ->exception( ... ) method.

Note that most of the outer methods in the module will try to catch
exceptions, so the log object itself, will throw an exception to stop
excecution of the import, when it receives a serious enough exception
itself.

== FAQ ==

=== Where do I put my plugins ===

In any module, as long as the module has indicated that it has the plugins using
the hook_ctools_plugin_directory() hook.
