<?php
/**
 * @file SelfLoggingClassLog.inc The Import Queue Log plugin that lets objects log themselves
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('Self-Logging Class logger'),
  'class' => 'ImportQueue_Log_Backend_SelfLogger',

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_MESSAGE
  )
);


/**
 * @interface ImportQueue_Log_SelfLogger This class can handled it's own log events
 *
 * This interface indicates to the logging system, that a class passed is willing to
 * also handle logging events.  This interface is actually only used by a single log
 * plugin to look at data passed to the log method, and any class implmenting this
 * interface may receive a log() call.
 */
interface ImportQueue_Log_SelfLogger {

  /**
   * @function Log a message in the Import Queue System
   *
   * @param $message for the log entry
   * @param $severity integer from 1-10
   * @return a boolean, indicating success
   */
  public function log($message, Array $data, $severity);

}

/**
 * @class ImportQueue_Log_Backend_SelfLogger
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_SelfLogger extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message using dpm
   */
  public function log($message, Array $data, $severity) {
    if ($this->checkSeverity($severity)) {

      foreach($data as $key=>$value) {
        if ( is_object($value) && ($value instanceof ImportQueue_Log_SelfLogger) ) {
          $value->log($message, $data, $severity);
        }
      }

    }
  }

}

