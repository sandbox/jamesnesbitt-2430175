<?php
/**
 * @file DPMLog.inc The Import Queue Log plugin that outputs to the DrupalDebug Log
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('Drupal Debug Logging'),
  'class' => 'ImportQueue_Log_Backend_DrupalDebug',

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_MESSAGE
  )
);

/**
 * @class ImportQueue_Log_Backend_dpm
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_DrupalDebug extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message using dpm
   */
  public function log($message, Array $data, $severity) {
    if (
      $this->checkSeverity($severity)
      && module_exists('devel')
    ) {
      dpm($data, '['.$severity.'] '.$message);
    }
  }

}
