<?php
/**
 * @file ImportQueue_TaskBase.inc An abstract base plugin for other task plugins
 *
 */

/**
 * @abstract @class ImportQueue_TaskBase Optional base class
 *
 * An optional base class that can be used as a starting point
 * for your plugins
 *
 * @extends ImportQueue_plugin_BaseWState to get the constructor, adminForm and state methods
 * @implements ImportQueue_Plugin actually done in the parent class, but marked here to be more visible
 * @implements ImportQueue_Stateful this plugin maintains a state (actually done in the parent class)
 * @implements ImportQueue_Task to mark it a task plugin
 */
abstract class ImportQueue_TaskBase extends ImportQueue_plugin_BaseWState implements ImportQueue_Plugin, ImportQueue_Task, ImportQueue_Stateful {

  /**
   * Add a new item to the Queue
   *
   * @param String item type plugin handler for the item
   * @param ImportQueue_DataStruct data for the item
   * @param Integer $priority rating for the item
   *
   * @returns Queue::addItem response (boolean success)
   */
  protected function addToQueue($type, ImportQueue_DataStruct $data, $priority=5) {
    $data->priority = $priority;
    $id = static::id($data);
    return importqueue_queue_item_add($type, $id, $data, $this->log);
  }
  /**
  * Create a hash based ID for an item that has no uniqueID
  */
  static protected function id(ImportQueue_DataStruct $data) {
    return drupal_hash_base64(serialize($data->meta())); // @TODO decide if there is something less heavy
  }

}
