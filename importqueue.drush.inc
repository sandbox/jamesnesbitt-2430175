<?php
/**
 * Implements hook_drush_command().
 */
function importqueue_drush_command() {
  $items = array();

  $items['importqueue-items-batch'] = array(
    'description' => 'Batch process queued items',
    'arguments'   => array(
      'limit'    => dt('Batch limit'),
    ),
    'aliases' => array('iq-ib'),
  );
  $items['importqueue-tasks-batch'] = array(
    'description' => 'Batch import Task plugins.',
    'options' => array(
      'tasks' => 'Specify which task plugins to run (comma-delimited list).',
    ),
    'aliases' => array('iq-tb'),
  );

  return $items;
}

function drush_importqueue_items_batch($limit = 10) {
  importqueue_loadincludes('batch', 'log');

  $batch = importqueue_items_batch($limit, array('status'=>ImportQueue_Queue::STATUS_QUEUED), 'drush');
  batch_set($batch);
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();

  return 'ImportQueue items imported : '.implode(', ',$tasks);
}

function drush_importqueue_tasks_batch() {
  importqueue_loadincludes('batch','log');

  $tasks = drush_get_option('tasks');
  if (is_null($tasks)) {
    $settings = importqueue_plugintype_settings_get('importqueue_task', NULL, $log);
    $tasks = $settings['default_batch_tasks'];
  }
  else {
    $tasks = explode(',',$tasks);
  }

  $batch = importqueue_tasks_batch($tasks, 'drush');
  batch_set($batch);
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();

  return 'ImportQueue tasks imported : '.implode(', ',$tasks);
}
