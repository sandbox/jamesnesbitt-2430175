<?php
/**
 * @file WatchdogLog.inc The Import Queue Log plugin for Drupal Watchdog
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('Watchdog Loggging'),
  'class' => 'ImportQueue_Log_Backend_watchdog',

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_EVENT
  )
);

/**
 * @class ImportQueue_Log_Backend_watchdog
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_watchdog extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message to the Drupal watchdog system
   */
  public function log($message, Array $data, $severity) {
    if ($this->checkSeverity($severity)) {

      switch($severity) {
        case 1:
        case 2:
        case 3:
          $watchdog_severity = WATCHDOG_ERROR;
          break;
        default:
          $watchdog_severity = WATCHDOG_NOTICE;
      }

      // use any simple variables from data as watchdog vars
      $variables = array();
      foreach($data as $key=>$value) {
        switch(TRUE) {
          case is_array($value):
          case is_object($value):
            continue 2;

          default:
            $variables[$key] = $value;
        }
      }

      // pass the log entry to watchdog
      watchdog('importqueue', $message, $variables, $watchdog_severity);

    }
  }


  static public function logger_form(&$form, &$form_state, Array $settings, ImportQueue_Log $log) {

    $form['MinimumSeverity'] = array(
      '#type' => 'textfield',
      '#title' => t('Severity'),
      '#size' => 5,
      '#description' => t('Any messages received with larger severity index than this number will be ignored'),
      '#default_value' => isset($settings['MinimumSeverity']) ? $settings['MinimumSeverity'] : ImportQueue_Log::SEVERITY_ERROR
    );

  }

}
