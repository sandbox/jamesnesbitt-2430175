<?php
/**
 * @file
 *
 */

/**
 * @class ImportQueue_ItemBase An base item class
 *
 * @implements ImportQueue_Plugin to be a plugin
 * @implements ImportQueue_Item to be an item plugin
 */
abstract class ImportQueue_ItemBase extends ImportQueue_Plugin_Base implements ImportQueue_Plugin, ImportQueue_Item {

  /**
   * Run the migration of a single queue item through the BatchAPI
   *
   * @param $item item configuration, likely data from the queue
   * @param $context a BatchAPI context array, or simulation
   *
   * @return boolean success
   *
   * @see BatchAPI
   *
   * @NOTE you may directly access the log object for more precise logging
   */
  abstract public function run(ImportQueue_Queue $item, Array &$context);

}
