<?php
/**
 * @file DataStruct.inc An adaptable object for handling data and metadata combinations
 *
 */

/**
 * @class ImportQueue_DataStruct a simple struct type object for holding array data with metada
 *
 * This is a simple reusable implementation that gives you array data, with metadata access
 *
 * @implements ArrayAccess for access to the actual data
 * @implements Iterator for looping through data
 * @implements Serializable so that data can be saved fully serialized in queue items, or anywhere else
 *
 * @note access metadata through object accessors
 * @note get array of ->meta and array of ->data()
 * @note it is handy if the ImportQueue_Log instance used is also a serializable object, if you want to serialize this object
 */
class ImportQueue_DataStruct extends ArrayObject implements ImportQueue_Plugin, ImportQueue_Tool, Serializable {

  protected $log = NULL;

  public $meta = array();   // MetaData for item
  public $data = array();     // an array of actual Item Data

  static public function __init(Array $info, Array $options, ImportQueue_Log $log) {
    // we don't use any init information
  }

  public function __construct(Array $options, ImportQueue_Log $log) {
    $this->log = $log;

    // pull data and metadata from the options
    if (isset($options['meta'])) { $this->meta = static::makeIterator($options['meta']); }
    parent::__construct(isset($options['data']) ? $options['data'] : array());
  }
  static protected function makeIterator($source) {
    if (is_array($source)) {
      return new ArrayIterator($source);
    }
    if (is_object($source)) {
      return new ArrayObject($source);
    }
    return array();
  }

  // create a data struct from separate arrays
  static public function get(Array $meta, Array $data, ImportQueue_Log $log=NULL) {
    if (empty($log)) {
      $log = importqueue_log_get();
    }

    $options = array(
      'meta' => $meta,
      'data' => $data
    );
    return new ImportQueue_DataStruct(array(), $options, $log);
  }

  /**
   * Serializable part
   *
   * @see PHP Serializable
   */
  public function serialize() {
    $serialize = array(
      'meta' => $this->meta,
      'data' => $this->data,
    );
    if ($this->log instanceof Serializable) {
      $serialize['log'] = $this->log;
    }
    return serialize($serialize);
  }
  public function unserialize($serialized) {
    $unserialized = unserialize($serialized);
    $this->meta = $unserialized['meta'];
    $this->data = $unserialized['data'];
    if (isset($unserialized['log'])) {
      $this->log = $unserialized['log'];
    }
    else {
      $this->log = importqueue_log_get();
    }
  }

  /**
   * Get Meta data about queue item
   */

  /**
   * Retrieve ItemData meta data/properties
   *
   * This is done using the accessor method, only to abstract
   * what private properties the itemData struct has, so that class
   * can change as makes sense, without needing to change this
   * class.
   *
   * @note technically you could use this to retrieve the data array?
   */
  public function __get($key) {
    if (isset($this->meta[$key])) {
      return $this->meta[$key];
    }
  }
  public function __set($key, $value) {
    $this->meta[$key] = $value;
  }
  public function __isset($key) {
    return isset($this->meta[$key]);
  }
  public function __unset($key) {
    if (isset($this->meta[$key])) {
      unset($this->meta[$key]);
    }
  }

  /**
   * Access the ItemData actual data as an array
   *
   * @see PHP ArrayAccess
   * @note ArrayAccess Interface implementation accesses the itemdata->data array
   */

  /**
   * Retrieve the keys for stored information
   */
  public function keys() {
    return array_keys($this->data());
  }
  public function metaKeys() {
    return array_keys($this->meta());
  }


  /**
   * Retrieve array copies of stored information
   */
  public function meta() {
    return $this->meta->getArrayCopy();
  }
  public function data() {
    return $this->getArrayCopy();
  }

}
