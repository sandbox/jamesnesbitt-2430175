<?php
/**
 * @file DPMLog.inc The Import Queue Log plugin for Drupal devel dpm()
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('DPM Loggging (Requires devel module)'),
  'class' => 'ImportQueue_Log_Backend_dpm', // tell ctools what class to use

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_EVENT
  )
);

/**
 * @class ImportQueue_Log_Backend_dpm
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_dpm extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message using dpm
   */
  public function log($message, Array $data, $severity) {
    if (
      $this->checkSeverity($severity)
      && module_exists('devel')
    ) {
      dpm($data, '['.$severity.'] '.$message);
    }
  }

}
