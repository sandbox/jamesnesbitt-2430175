<?php
/**
* @file item.inc Import Queue Item plugins architecture
*
* This file contains all of the functionality and abstract
* architecture for the Import Queue Item plugins.
*
* The plugins themselves are impements as CTOOLs plugins
* that implement the ImportQueueItem interface.
*
* There are multiple optional base item classes here:
* - ImportQueueItemBase : a bare implentation of the interface
* - ImportQueueItemBase_SOAP : an extension of the base base that
*        also has some SOAP capabilities
*/

/**
 * Run a single Import Queue item by passing the item to the appropriate plugin
 *
 * @param $item the item configuration (likely an array, likely from the queue)
 * @param $options an array of options to pass to the plugin constructor
 *
 * @return boolean success
 */
function importqueue_item_run(ImportQueue_Queue $item, ImportQueue_Log $log=NULL, Array &$context=NULL) {
  importqueue_loadincludes('item', 'batch', 'log');
  if (empty($log)) {
    $log = importqueue_log_get();
  }

  try {

    $log->message('Queue item', array('id'=>$id, 'item'=>$item), ImportQueue_Log::SEVERITY_DEBUG1);

    if (!(is_object($item) && ($item instanceof ImportQueue_Queue))) {
      throw new ImportQueue_ExceptionError('Item import function was handed an invalid item', array('item'=>$item));
    }

    $data = $item->data();

    // determine what plugin type to use.
    $type = _importqueue_item_getpluginname($item);
    // get an instance of the plugin
    $plugin = importqueue_plugin_get('item', $type, array(), $log);

    if (is_object($plugin) && ($plugin instanceof ImportQueue_Item)) {
      $log->message('Retrieved plugin instance', array('plugin'=>$plugin), ImportQueue_Log::SEVERITY_DEBUG1);

      if (empty($context)) {
        $context = array(
          'finished' => 0,
          'sandbox' => array()
        );
        $log->message('Created fake batchAPI context', array('context'=>$context), ImportQueue_Log::SEVERITY_DEBUG1);
      }

      $log->message('Starting item plugin run', array('item'=>$item, 'context'=>$context), ImportQueue_Log::SEVERITY_MESSAGE);
      if ($plugin->run($item, $context)) {
        $log->message('Finished item plugin run() : SUCCESSFUL', array('item'=>$item, 'context'=>$context), ImportQueue_Log::SEVERITY_MESSAGE);

        // close the item if it is left as claimed, maybe the plugin forgot to close it.
        if ($item->status()==ImportQueue_Queue::STATUS_CLAIMED) {
          $log->message('The item plugin forgot to close the queue item.  Closing it now.', array('item'=>$item, 'context'=>$context), ImportQueue_Log::SEVERITY_MESSAGE);
          $item->close();
        }

        return TRUE;
      }
      else {
        $log->message('Finished item plugin run() : FAILED', array('item'=>$item, 'context'=>$context), ImportQueue_Log::SEVERITY_ERROR);
        $item->release();
      }

    }
    else {
      $log->message('could not retrieve plugin instance : '.$plugin, array('type'=>$type,'plugin'=>$plugin), ImportQueueLog::SEVERITY_ISSUE);
    }

    // unset the item, to run it's destructor
    unset($item);

  } catch (Exception $e) {
    $log->exception($e);

    // release any unprocessed items
    // @NOTE techincally, the item destructor should release any items not closed, but we do it explicitly here anyway because this is PHP, and we don't trust destructors.  Do we?
    if (is_object($item) && ($item instanceof ImportQueue_Queue) && $item->status==ImportQueue_Queue::STATUS_CLAIMED) {
      $item->release();
    }
  }

  return FALSE;
}

/**
 * Make a stub entity, by passing data to an item plugin that can make stubs
 */
function importqueue_item_createstub($plugin, ImportQueue_DataStruct $data, ImportQueue_Log $log=null) {

  $info = importqueue_plugin_info('item', $plugin);
  $class = $info['class'];
  if (in_array('ImportQueue_Item_StubHandler', class_implements($class))) {
    return $class::createStub($data, $log);
  }
  else {
    throw new ImportQueue_Exception_Item_NotAStubHandler('Cannot create a stub entity, as the item plugin is not a stub handler (does not implement ImportQueue_Item_StubHandler.)', array('plugin'=>$plugin, 'data'=>$data, 'info'=>$info));
  }

}

/**
 * Determine what item plugin name should be used for an item (likely a queue item)
 *
 * this little internal function is here because it is not decided what the nature of the
 * item kept in the queue is going to be.  Is it an array?
 *
 * @param $item item , most likely from the queue
 *
 * @returns string type that should match an item plugin
 */
function _importqueue_item_getpluginname($item) {
  $data = $item->data();
  return $data->type;
}

/**
 * @interface ImportQueue_Item The bare Import Queue item plugin interface
 */
interface ImportQueue_Item {

  /**
   * run the import plugin
   *
   * @param ImportQueue_Queue $item to be processed
   * @param Array $context from BatchAPI
   *
   * @note while this method is not heavily batchAPI dependent, it will likely
   *    be run in a Batch context, and may want to the context, for example to
   *    set the message.
   *
   * @return boolean success
   */
  public function run(ImportQueue_Queue $item, Array &$context);

}

/**
 * @interface ImportQueue_Item_StubHandler marks an item plugin that can create stubs
 */
interface ImportQueue_Item_StubHandler {

  /**
   * This function can be called to create a stub entity
   *
   * @param ImportQueue_DataStruct a struct with all the information required to create a stub
   * @param ImportQueue_Log a logger (as this is a static method)
   */
  static public function createStub(ImportQueue_DataStruct $data, ImportQueue_Log $log);

}

/**
 * Exceptions
 */

/**
 * @exception ImportQueue_Exception_ItemSourceFailure Item Failed to retrieve data from source
 */
class ImportQueue_Exception_ItemSourceFailure extends ImportQueue_ExceptionIssue { }

/**
 * @exception ImportQueue_Exception_ItemActionExceptionOccurred Generic Handler error in class
 */
class ImportQueue_Exception_ItemActionExceptionOccurred extends ImportQueue_ExceptionIssue { }

/**
 * @exception ImportQueue_Exception_Item_UnknownAction Queue item asked for an unknown action to be executed
 */
class ImportQueue_Exception_Item_UnknownAction extends ImportQueue_ExceptionIssue { }

/**
 * @exception ImportQueue_Exception_Item_NotAStubHandler The system was asked to create a stub using an item plugin that is not a stub handler
 */
class ImportQueue_Exception_Item_NotAStubHandler extends ImportQueue_ExceptionIssue { }
