<?php

function vub_import_admin_test_queuetest(VUBImport_Log $log, $args) {
  vub_import_loadincludes('queue');

  $info = vub_import_plugin_info('vub_import_queuebackend', NULL, $log);
  $log->message('Queue settings loaded', array('info'=>$info), VUBImport_Log::SEVERITY_DEBUG3);

  $queue = vub_import_queueplugin_get();

  drupal_set_title('Test the Queue');

  $output = array(
    '#type' => 'fieldset',
    '#title' => t('Queue Test'),
    '#description' => t('This test will test the active queue, by adding items, and pulling items from the queue.  It will also test filtering the queue, and saving queue items.  This test will not test clearing the queue.'),

    'settings' => array(
      '#prefix' => '<dl>',

      'activequeue' => array('#markup'=> '<dt>Active Queue Plugin</dt><dd></dd>'),

      '#suffix' => '</dl>'
    ),

  );

  /**
   * Test Adding a new queue item
   */
  $type = 'TestItem';
  $id = '1234567890';
  $item_meta = array(
    'uuid' => 'abcdefghijklmnopqrstuvwxyz',
    'meta1'=>'meta1',
    'meta2'=>'meta2',
    'meta3'=>'meta3',
  );
  $item_data = array(
    'string' => "String",
    'integer' => 100,
    'array' => array('1', '2', '3'),
  );
  $data = vub_import_plugin_get('vub_import_tool', 'DataStruct', array('meta'=>$item_meta, 'data'=>$item_data), $log);
  $item = vub_import_queue_item_add($type, $id, $data, $log);
  $item_meta_info = $item->data()->meta();

  // build the output report.
  $output['test_add'] = array(
    '#type' => 'fieldset',
    '#title' => t('TEST: Add an item to the queue'),

    'settings' => array(
      '#prefix' => '<h3>Settings</h3><dl>',

      'id' => array('#markup'=>'<dt>id</dt><dd>'.var_export($id,true).'</dd>'),
      'meta' => array('#markup'=>'<dt>meta</dt><dd>'.var_export($item_meta,true).'</dd>'),
      'data' => array('#markup'=>'<dt>data</dt><dd>'.var_export($item_data,true).'</dd>'),

      '#suffix' => '</dl>',
    ),
    'tests' => array(
      '#prefix' => '<h3>Created Item</h3><dl>',

      'item_id' => array('#markup' => '<dt>Queue Item ID</dt><dd>'.$item_meta_info['id'].'</dd>'),
      'meta' => array('#markup'=>'<dt>meta</dt><dd>'.var_export($item->data()->meta(),true).'</dd>'),
      'data' => array('#markup'=>'<dt>data</dt><dd>'.var_export($item->data()->data(),true).'</dd>'),

      '#suffix' => '</dl>',
    ),
  );

  /**
   * Testing queue list
   */

  // build the output report.
  $output['test_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('TEST: List Queue Items'),

    'filters' => array(
      '#prefix' => '<h3>Filters</h3><dl>',

      '#suffix' => '</dl>',
    ),

    'list' => array(
      '#prefix' => '<h3>Item List</h3><ul>',

      '#suffix' => '</ul>',
    ),
  );

  $filters = array(
    'limit' => 5
  );
  $list = $queue->listItems($filters);
  foreach($filters as $key=>$value) {
    $output['test_list']['filters'][$key] = array('#markup'=>'<dt>'.$key.'</dt><dd>'.var_export($value,true).'</dd>');
  }
  foreach($list as $id=>$item) {
    $output['test_list']['list'][$id] = array('#markup'=>'<li>'.$id.': '.var_export($item->data()->meta(), true).'</li>');
  }

  /**
   * Test Retrieving our item
   */
  $filters = array(
    'uuid' => 'abcdefghijklmnopqrstuvwxyz',
    'limit' => 1
  );
  $list = $queue->listItems($filters);

  // build the output report.
  $output['test_retrieve'] = array(
    '#type' => 'fieldset',
    '#title' => t('TEST: Retrieve our created item (filter)'),

    'filters' => array(
      '#prefix' => '<h3>Filters</h3><dl>',

      '#suffix' => '</dl>',
    ),

    'list' => array(
      '#prefix' => '<h3>Item List</h3><ul>',

      '#suffix' => '</ul>',
    ),
  );
  foreach($filters as $key=>$value) {
    $output['test_retrieve']['filters'][$key] = array('#markup'=>'<dt>'.$key.'</dt><dd>'.var_export($value,true).'</dd>');
  }
  foreach($list as $id=>$item) {
    $output['test_retrieve']['list'][$id] = array('#markup'=>'<li>'.$id.': '.var_export($item->data()->meta(), true).'</li>');
  }

  /**
   * Test changing the item status
   */

  // build the output report.
  $output['test_statuschange'] = array(
    '#type' => 'fieldset',
    '#title' => t('TEST: Change the retrieved item status'),

    'settings' => array(
      '#prefix' => '<h3>Settings</h3><dl>',

      'id' => array('#markup'=>'<dt>id</dt><dd>'.var_export($item_meta_info['id'],true).'</dd>'),

      '#suffix' => '</dl>',
    ),
    'tests' => array(
      '#prefix' => '<h3>Created Item</h3><dl>',
      '#suffix' => '</dl>',
    ),
  );

  /**
   * @todo Probably we should actually fetch the item again, instead of just relying on the ->status(), but I checked it manually using FileQueue
   */
  $output['test_statuschange']['settings']['status'] = array('#markup' => '<dt>Status before claim()</dt><dd>'.$item->status().'</dd>');
  $item->claim();
  $output['test_statuschange']['tests']['claim'] = array('#markup' => '<dt>Status after claim()</dt><dd>'.$item->status().'</dd>');
  $item->release();
  $output['test_statuschange']['tests']['release'] = array('#markup' => '<dt>Status after release()</dt><dd>'.$item->status().'</dd>');
  $item->close();
  $output['test_statuschange']['tests']['closed'] = array('#markup' => '<dt>Status after close()</dt><dd>'.$item->status().'</dd>');

  /**
   * Test saving a changed item
   */

  // build the output report.
  $output['test_change'] = array(
    '#type' => 'fieldset',
    '#title' => t('TEST: Change a queue item'),

    'settings' => array(
      '#prefix' => '<h3>Item</h3><dl>',

      'id' => array('#markup'=>'<dt>id</dt><dd>'.var_export($id,true).'</dd>'),
      'meta' => array('#markup'=>'<dt>meta</dt><dd>'.var_export($item_meta,true).'</dd>'),
      'data' => array('#markup'=>'<dt>data</dt><dd>'.var_export($item_data,true).'</dd>'),

      '#suffix' => '</dl>',
    ),
    'tests' => array(
      '#prefix' => '<h3>Changed Data</h3><dl>',

      '#suffix' => '</dl>',
    ),
  );
  $item->data()->meta4 = 'meta4';
  $output['test_change']['tests']['add_meta'] = array('#markup'=>'<li>Added meta4: '.var_export($item->data()->meta(), true).'</li>');
  $item_get_data = $item->data();
  $item_get_data['added'] = 'meta4';
  $output['test_change']['tests']['add_data'] = array('#markup'=>'<li>Added data: '.var_export($item->data()->data(), true).'</li>');
  $item->save();


  return $output;
}

/**
 * VUB Import administrator switchboard
 *
 * @deprecated this used to be a good place to put temp functions
 *    instead of adding menus,  Not the menu system is pretty Completel
 *
 * @todo get rid of me
 */
function vub_import_admin_switchboard( $action='settings' ) {
  $args = func_get_args();
  $action = array_shift($args);

  vub_import_loadincludes();
  $log = vub_import_log_get();

  switch ($action) {

    case 'manage':
      return drupal_get_form('vub_import_management');

    default:
      return drupal_get_form('vub_import_settings');

  }
}
