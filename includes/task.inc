<?php
/**
 * @file task.inc Import Queue task abstraction
 *
 * This file contains all of the Import Queue Task functionality
 * and base/abstract architecture.
 *
 * Task plugins are actually ctools plugins that will include a single
 * new custom class that uses the ImportQueueTask interface.
 *
 * These plugins are loaded using importqueue_plugin_get()
 *
 * @SEE importqueue_plugin_get()
 */

 /**
 * Run a single Import Queue task
 *
 * @param String $plugin the task name, which should correspond to a plugin
 * @param Array $options an array of options to pass to the plugin constructor
 * @param Array Optional &$context aray from the Drupal BatchAPI
 * @param Variable Log definition, either a string key, or a config array (or admin log will be used)
 *
 * @throws ImportQueue_ExceptionError if an unknown task plugin was requested
 *
 * @return boolean success
 */
function importqueue_task_run($plugin, $options=array(), &$context=NULL, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes('task');
  if (empty($log)) {
    $log = importqueue_log_get('admin');
  }

  try {

    $object = importqueue_plugin_get('task', $plugin, $options, $log);
    if (!($object instanceof ImportQueue_Plugin)) {
      throw new ImportQueue_ExceptionError('Unknown task plugin execution requested "'.$plugin.'"', array('task'=>$object));
    }

    $log->message('Retrieved plugin instance : '.$plugin, array('plugin'=>$object), ImportQueue_Log::SEVERITY_DEBUG1);

    if (!is_array($context)) {
      // fake a batchAPI context
      $context = array(
        'finished' => 0,
        'sandbox' => array()
      );
      $log->message('Created fake batchAPI context', array('context'=>$context), ImportQueue_Log::SEVERITY_DEBUG2);
    }
    $log->message('Running plugin->run()', array(), ImportQueue_Log::SEVERITY_DEBUG2);
    $success = $object->run($context);
    $log->message('Finished plugin->run()', array(), ImportQueue_Log::SEVERITY_DEBUG2);

    unset($object);
    return $success;

  } catch (Exception $e) {
    // normall we would consider catching exceptions thrown by the logging system
    // to halt execution, but there is nothing left to do anyway.
    $log->exception($e);
    return false;
  }
}

/**
 * Form handler to configure tasks in general
 */
function importqueue_plugin_task_settings_form(&$form, &$form_state, $settings, $log) {

  $info = importqueue_plugin_info('task', NULL, $log);

  $plugin_options = array();
  $abstract_plugins = array();
  foreach($info['plugins'] as $key=>$plugin) {
    if (!(isset($plugin['abstract']) && $plugin['abstract'])) {
      $plugin_options[$key] = $plugin['label'].' ('.$key.')';
    }
    else {
      $abstract_plugins[$key] = $plugin['label'].' ('.$key.')';
    }
  }

  $form['default_batch_tasks'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Default batch tasks'),
    '#description' => t('These selected task plugins will be used as the default tasks, run during batches operations such as Drush, and Cron tasks.</p>'),
    '#options' => $plugin_options,
    '#default_value' => isset($settings['default_batch_tasks']) ? $settings['default_batch_tasks'] : array()
  );

  if (count($abstract_plugins)>0) {
    $form['disable_tasks'] = array(
      '#type' => 'markup',
      '#markup' => '<p>Note that the following plugins are marked as abstract, and are not available in the system for task operations, although they may still offer configuration forms: <ul><li>'.implode('</li><li>',$abstract_plugins).'</li></ul>',
    );
  }

}

/**
 * @interface VUBTask the bare Import Queue Task plugin interface
 *
 * This interface defines the minimum public requirements for
 * A Import Queue task plugin.  This gives you an idea of what
 * the import system expects the plugin to implement.
 *
 * @see ImportQueueTaskBase Optional task base class
 *
 * @todo extend the vubimport_plugin interace?
 */
interface ImportQueue_Task {

  /**
   * Execute the plugin on a context
   *
   * @param Array $context batchAPI context variable
   *
   * @NOTE for error-handling you may throw an exception, which can be caught by the stack
   */
  public function run(&$context);

}

/**
 * Exceptions
 */

/**
 * @exception ImportQueue_Exception_TaskSourceFailure Task Failed to retrieve data from source
 */
class ImportQueue_Exception_TaskSourceFailure extends ImportQueue_ExceptionIssue { }

