<?php
/**
 * @file DrushLog.inc The Import Queue Log plugin for Drush
 *
 * @see ImportQueue_Log_Backend
 */

// The Ctools plugin array
$plugin = array(
  'label' => t('Drush Loggging'),
  'class' => 'ImportQueue_Log_Backend_drushmessage', // tell ctools what class to use

  'defaults' => array(
    'MinimumSeverity' => ImportQueue_Log::SEVERITY_EVENT
  )
);

/**
 * @class ImportQueue_Log_Backend_drushmessage
 *
 * @see ImportQueue_Log_Backend
 */
class ImportQueue_Log_Backend_drushmessage extends ImportQueue_Log_Backend_Base {

  /**
   * Log the message to the Drush log
   */
  public function log($message, Array $data, $severity){

    if (
         $this->checkSeverity($severity)
      && function_exists('drush_log')
    ) {
      // determine some stuff
      switch($severity) {
        default:
          $type='notice';
          $error=FALSE;
      }

      // pass the message on to drush
      drush_log($message, $type, $error);
    }
  }

}
