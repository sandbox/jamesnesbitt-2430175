<?php
/**
 * @file plugin.inc base plugin classes
 *
 * All plugins loaded by this module plugin abstraction system
 * need to conform to interfaces defined in this include
 *
 * LOADING A PLUGIN
 *
 * To use a plugin, you can use the @see importqueue_plugin_get() function
 * in the .module file.
 * To get information about a plugin type, or a specific plugin you can
 * use the @see importqueue_plugin_info() function, also in the .module
 *
 * WHEN A PLUGIN IS LOADED
 *  1. It's information (plugin array or info file) is loaded from the
 *     the ctools system;
 *  2. It's info is 'processed' to check for parents, and includes. Those
 *     elements are loaded and processed before continuing;
 *  3. An instance of the main class for the plugin is created and
 *     returned.
 *
 * @see importqueue_plugin_build build a plugin instance from the plugin info
 * @see importqueue_plugin_getplugininfo retrieve ctools info for a type/plugin
 * @see _importqueue_plugin_info_process process type/plugin info
 * Plugins
 *
 * All of the module plugins are ctools loadable plugins.  Some
 * are .info style plugins, and some just use $plugin = array()
 * syntax.
 *
 * PLUGIN INFO
 *
 * Plugins have the following settings:
 *
 *  'label' => string used for labeling the plugin
 *
 *  'abstract' => Boolean indicating that this plugin is an abstract
 *     plugin and should never be instantiated directly.
 *     Often this is used for parent abstract classes, but it can
 *     also be used in cases where the plugin has classes designed
 *     to be used directly, and not loaded with the plugin builder,
 *  'class' => plugin object class (OPTIONAL if ABSTRACT) used to
 *     define what class instance should be created when creating
 *     a plugin.
 *
 *  'coreincludes' => array of strings indiciating which of the core
 *     includes for the system should be included.  These are the
 *     files in the /includes folder
 *  'includes' => an array of [plugin_type][plugin] that should be
 *     loaded before this one.  This is usually used if a plugin
 *     uses a class from another plugin, with actually loading the
 *     plugin (often a tool plugin.)
 *  'parents' => an array of plugins of the same type, that need
 *     to be loaded before this plugin, to provide parent classes
 *     and interfaces needed by this plugin
 *
 *
 * INTERFACES
 *
 * ImportQueue_Plugin : base import definition, actually only includes
 *    a constructor definition, so that we know how to create it.
 *
 * ImportQueue_Configurable : an interface indicating that the
 *    class uses the plugin options system, meaning that it provides
 *    an options form for control of settings.
 *
 * BASE CLASS
 *
 * ImportQueue_Plugin_Base : an abstract plugin that implements the base
 *    ImportQueue_Plugin constructor, keeping the options and log object
 *
 */

/**
 * Retrieve and build an instance of a particular ImportQueue ctools plugin.
 *
 * @TODO It would be appropriate to validate the class before creating an instance
 *
 * @param $type plugin type (task, item)
 * @param $name the plugin name (the name of the file without any .inc)
 * @param $options an array of values that override any plugin settings (which include plugin defaults)
 *     @NOTE the options array can be considered as an override for default plugin values, and
 *       save plugin settings.  Default values come from the plugin array, and settings come
 *       from a drupal variable, with an admin UI
 *
 * @returns an instance of the plugin class, constructed by passing in the options
 */
function importqueue_plugin_build(Array $plugin, Array $options = array(), ImportQueue_Log $log) {

  if (isset($plugin['abstract']) && $plugin['abstract']) {
    // this is an abstract plugin, not meant to be instantiated

    /** @TODO throw exception ? */
  }
  else if ( is_array($plugin) && class_exists($plugin['class'])) {
    // create an instance of the plugin class
    $class = $plugin['class'];

    // add the plugin defaults to the plugin settings
    if ( isset($plugin['defaults']) ) {
      $options += $plugin['defaults']; // add the options to the plugin array
    }

   return new $class($options, $log);
  }
}

/**
 * Return plugin info array for a single plugin, or a plugin type
 *
 */
function importqueue_plugin_getplugininfo($type, $plugin=null, ImportQueue_Log &$log) {
  importqueue_loadincludes();

  ctools_include('plugins');
  $types =& drupal_static(__function__, array());

  if (empty($types)) {
    $types = importqueue_ctools_plugin_type(); // @TODO get this from ctools instead of locally to allow for alters?
    foreach(array_keys($types) as $key) {
      $types[$key]['type'] = $key; // add the type name to the type info
    }
  }

  if (!isset($types[$type])) {
    //* Bad plugin type
    return;
  }

  // get plugin list for type
  if (empty($types[$type]['plugins']) ) {
    $types[$type]['plugins'] = (array)ctools_get_plugins('importqueue', $type);
    foreach(array_keys($types[$type]['plugins']) as $id) {
      $types[$type]['plugins'][$id]['type'] = $type;
      $types[$type]['plugins'][$id]['id'] = $id;
    }
  }

  if (empty($plugin)) { // return all plugin info
    /**
    * @note if you load the info for a plugin type, then the class files
    * for the plugins of that type are not loaded.  You need to load the
    * info for each plugin before you can use it.
    * This is intentional, to prevent a lot of unnecessary PHP, if the
    * classes are not going to be used
    * You will still get a list of plugins, and some of the plugin info
    * BUT not the plugin settings.
    */
    _importqueue_plugin_info_process($types[$type], NULL, $log);
    return $types[$type];
  }
  else {
    _importqueue_plugin_info_process($types[$type], $plugin, $log);
    if (isset($types[$type]['plugins'][$plugin])) {
      return $types[$type]['plugins'][$plugin];
    } else {
      return NULL;
    }
  }
}
/**
 * Process plugin info, and type information, before returning plugin info
 *
 * This is an attempt to abstact plugin dependencies etc, allowing plugin types
 * and plugins to declare includes, dependencies and maybe an abstract tool
 * concept, or an additional plugin.
 *
 * @TODO optimize this with caching/static vars to prevent duplicate processing
 */
function _importqueue_plugin_info_process(Array &$type_info, $plugin, ImportQueue_Log &$log) {
  if (!isset($type_info['#processed'])) {

    // we mark it as processed to prevent infinite loops
    $type_info['#processed'] = true;

    // get any settings for the plugin
    $type_info['settings'] = importqueue_plugintype_settings_get($type_info['type'], NULL, $log);

    // load any required includes
    if (isset($type_info['coreincludes'])) {
      call_user_func_array( 'importqueue_loadincludes', $type_info['coreincludes']);
    }
  }

  if (
       !empty($plugin)
    && isset($type_info['plugins'][$plugin])
    && !isset($type_info['plugins'][$plugin]['#processed'])
  ) {
    $plugin_info =& $type_info['plugins'][$plugin];

    // we mark it as processed to prevent infinite loops
    $plugin_info['#processed'] = true;

    // load any parent plugins, so that we can have dependency chains
    if (isset($plugin_info['parents'])) {
      foreach((array)$plugin_info['parents'] as $parent) {
        importqueue_plugin_info($type_info['type'], $parent, $log); // retrieve the plugin array from the plugin file
      }
    }

    // load any required include plugins
    if (isset($plugin_info['includes'])) {
      foreach($plugin_info['includes'] as $type=>$plugins) {
        foreach($plugins as $id=>$include) {
          importqueue_plugin_info($type, $include, $log); // retrieve the plugin array from the plugin file
        }
      }
    }

    // load any required core include
    if (isset($plugin_info['coreincludes'])) {
      call_user_func_array( 'importqueue_loadincludes', (array)$plugin_info['coreincludes']);
    }

    // get any settings for the plugin
    $plugin_info['settings'] = importqueue_plugin_settings_get($plugin_info['type'], $plugin_info['name'], $log);

    // this plugin type uses an info file, include it's base include
    if (isset($type_info['info file'])) {
      if (!isset($plugin_info['classfile'])) {
        // rewrite the file from .info to .inc for default include
        $plugin_info['classfile'] = substr($plugin_info['file'], 0, -4).'inc';
      }
      require($plugin_info['path'].'/'.$plugin_info['classfile']);
    }

    // initialize the plugin
    ctools_plugin_get_class($plugin_info, 'class');
    $class = $plugin_info['class'];
    if (!empty($class)) {
      if (class_exists($class)) {
          $class::__init($plugin_info, $plugin_info['settings'], $log);
      }
      else {
        $log->message('Plugin defined class does not exist', array('info'=>$plugin_info), ImportQueue_Log::SEVERITY_WARNING);
      }
    }

  }

  return true;
}

/**
 * PLUGIN INTERFACES AND BASE CLASSES
 */

/**
 * @interface ImportQueue_Plugin
 */
interface ImportQueue_Plugin {

  /**
   * Plugin initializor, sort of like a static constructor
   *
   * @param Array $info informatipn array, from ctools.  Usefully to access meta data about the plugin.
   * @param Array $options array, from the plugin array, combined with saved settings for the plugin
   */
  static public function __init(Array $info, Array $options, ImportQueue_Log $log);

  /**
   * Plugin constructor, to configure and pass in a Logging object
   *
   * @param Array $options array, from the plugin array, combined with saved settings for the plugin
   * @param ImportQueue_Log $log Import Queue logging object
   */
  public function __construct(Array $options, ImportQueue_Log $log);

}

/**
 * @interface ImportQueue_Configurable object has configurable options
 *
 */
interface ImportQueue_Configurable {

  /**
   * Admin Settings form handler for the plugin
   *
   * @param $form drupal form handler form array for just
   * @param $form_state drupal form handler form state array
   *
   * @return none, modify the form array directly
   */
  static public function options_form(Array &$form, &$form_state, ImportQueue_Log $log);

}

/**
 * @class ImportQueue_Plugin_Base
 *
 * @implements @ImportQueue_Plugin
 */
abstract class ImportQueue_Plugin_Base implements ImportQueue_Plugin {

  /**
   * Keep the plugin info array
   *
   * @note this provides plugin information which is used to interact with plugin
   *   functions like settings_get() and state_get() etc.
   */
  static protected $info = array();

  /**
   * Keep the passed in settings, provided buy the plugin manager
   *
   * There are two vars here:
   * $plugin_options - contains the settings for the plugin as saved w/ defaults
   * $options will also contain any instance overrides (on top of the plugin_options)
   */
  static protected $plugin_options = array();
  protected $options = array(); // instance options

  /**
   * Keep the ImportQueue_Log object, for logging
   */
  protected $log = NULL;

  /**
   * Plugin initializer, to configure the class
   *
   * @param Array $info information array, from ctools.  Usefully to access meta data about the plugin.
   * @param Array $options array, from the plugin array, combined with saved settings for the plugin
   */
  static public function __init(Array $info, Array $options, ImportQueue_Log $log) {
    /**
     * reference placeholder (PHP loveliness)
     * If you don't assign a static::$property to a reference, then it
     * will be the same property across all children classes, even though
     * it should be statically late bound.
     *
     * @see https://bugs.php.net/bug.php?id=54543
     */

    $inforef = TRUE;
    static::$info =& $inforef;
    static::$info = $info;

    $optionsref = TRUE;
    static::$plugin_options =& $optionsref;
    static::$plugin_options = $options;
  }
  /**
   * Plugin constructor, to configure the instance and pass in a Logging object
   *
   * @param Array $options array, from the plugin array, combined with saved settings for the plugin
   * @param ImportQueue_Log $log Import Queue logging object
   */
  public function __construct(Array $options, ImportQueue_Log $log) {
    $this->options = drupal_array_merge_deep(static::$plugin_options, $options);
    $this->log = $log;
  }

  /**
   * @implements ImportQueue_Configurable
   */

  /**
   * Admin Settings form handler for the plugin
   *
   * @param $form drupal form array for just this admin form (not the whole form)
   * @param $form_state drupal form handler form state array
   *
   * @return hmmm
   */
  static public function options_form(Array &$form, &$form_state, ImportQueue_Log $log) {
    // By default have no administration form

    $form['no settings'] = array(
      '#markup' => '<strong>This plugin has no settings</strong>'
    );

    return $form;
  }

  /**
   * Save the current settings/options
   */
  protected function settings_save() {
    return importqueue_plugin_settings_set(static::$info['type'], static::$info['id'], $this->options, $this->log);
  }

  /**
   * Reset the current settings/options
   */
  protected function settings_reset() {
    $this->state = array();
    return importqueue_plugin_settings_set(static::$info['type'], static::$info['id'], array(), $this->log);
  }

}

/**
 * Plugin settings
 *
 * This is a feature that allows any plugin to have settings
 * associated with it which will be stored in Drupal variables.
 *
 * @note The Drupal variable location is 1 variable per plugin, named
 * after the module containing the plugin.  This should allow
 * feature/export control at the module level.
 * @note Remember that the options_form method is static
 * and so doesn't have access to the class $options
 *
 * @TODO the settings could probably be optimized (cached, amalgamated)
 */

/**
 * Retrieve settings for a plugin type
 */
function importqueue_plugintype_settings_get($type, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, NULL, $log);
  $variable = 'importqueue_settings_'.$type;
  /**
   * @note that drupal_array_merge_deep causes duplicates with indexed
   *   arrays so we have to rely on keyed arrays (dicts) instead.
   */
  $settings = drupal_array_merge_deep(
    // add any plugin defaults from plugin info
    isset($info['defaults']) ? $info['defaults'] : array(),

    // retrieve settings from variable space
    variable_get($variable, array())
  );
  $log->message('Retrieving plugin type settings for '.$type , array('type'=>$type, 'info'=>$info, 'variable'=>$variable, 'settings'=>$settings), ImportQueue_Log::SEVERITY_DEBUG3);
  return $settings;
}
/**
 * Save settings for a plugin type
 */
function importqueue_plugintype_settings_set($type, Array $settings, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, NULL, $log);
  $variable = 'importqueue_settings_'.$type;
  if (empty($settings)) {
    $log->message('Resetting plugin type settings for '.$type , array('type'=>$type, 'info'=>$info, 'variable'=>$variable, 'settings'=>$settings), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_del($variable);
  }
  else {
    $log->message('Saving plugin type settings for '.$type , array('type'=>$type, 'info'=>$info, 'variable'=>$variable, 'settings'=>$settings), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_set($variable, $settings);
  }
}

/**
 * Retrieve plugin settings, adding in plugin defaults if they exist
 *
 * @note plugins can have default settings, in their plugin definition
 *  under the key "defaults"
 */
function importqueue_plugin_settings_get($type, $plugin, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  /**
   * Keep a static copy of retrieved settings.
   *
   * @note we use a static copy because settings are inherited, so
   * everytime settings for a plugin are retrieved, it's parent's
   * settings are retrieved.  A static cache will reduce repeated
   * redundant array merges.
   */
  $settings =& drupal_static(__function__, array());

  if (!isset($settings[$type][$plugin])) {

    $info = importqueue_plugin_info($type, $plugin, $log);
    $module = $info['module'];
    $variable = $module.'_settings_'.$type.'_'.$plugin;

    /**
     * while a class can only have one parent, plugins can have multiple
     * which really only means multiple sources of inherited parent settings
     */
    $parents = array();
    if (isset($info['parents'])) {
      foreach($info['parents'] as $parent) {
        $parents = drupal_array_merge_deep($parents, importqueue_plugin_settings_get($type, $parent, $log));
      }
    }

    /**
    * Plugin defaults can come from one 2 places:
    *   1. The plugins parent's settings
    *   2. The plugin info['defaults'] (usually found in the info file or $plugin array)
    *   3. saved settings (saved by running the save function)
    *
    * @note that drupal_array_merge_deep causes duplicates with indexed
    *   arrays so we have to rely on keyed arrays (dicts) instead.
    */
    $settings[$type][$plugin] = drupal_array_merge_deep(
      // start with the parent plugin settings
      $parents,

      // add any plugin defaults from plugin info
      isset($info['defaults']) ? $info['defaults'] : array(),

      // retrieve settings from variable space
      variable_get($variable, array())
    );

    $log->message('Retrieving plugin settings for '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'settings'=>$settings[$type][$plugin]), ImportQueue_Log::SEVERITY_DEBUG3);
  }

  return $settings[$type][$plugin];
}
/**
 * Save plugin settings
 */
function importqueue_plugin_settings_set($type, $plugin, Array $settings=NULL, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $info = importqueue_plugin_info($type, $plugin, $log);
  //* @TODO unset any values that match defaults ?
  $module = $info['module'];
  $variable = $module.'_settings_'.$type.'_'.$plugin;
  if (empty($settings)) {
    $log->message('Resetting plugin settings for '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'settings'=>$settings), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_del($variable);
  }
  else {
    $log->message('Saving plugin settings for '.$plugin , array('type'=>$type, 'plugin'=>$plugin, 'info'=>$info, 'module'=>$module, 'variable'=>$variable, 'settings'=>$settings), ImportQueue_Log::SEVERITY_DEBUG1);
    return variable_set($variable, $settings);
  }
}
