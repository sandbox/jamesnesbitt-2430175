<?php
/**
 * @file queue.inc The Import Queue Queue architecture
 *
 * The Queue is used to separate tasks and items.  Tasks will populate
 * the queue with items.  Queue items will be pulled and used by item
 * plugins to process and create/update system entities.
 *
 * Data versus MetaData
 *
 * MetaData refers to the initial concepts of action, type and priority.
 * Of these, only type is really necessary as a concrete parameter, as it
 * directs the system to a specific type plugin; the others are arbitrary.
 *  - Type : item plugin
 *
 *  - UUID : some kind of unique system-wide identifier (will be generated if missing)
 *  - Priority (numeric 1-10, higher means more priority)
 *  - Action (insert, update, delete)
 *
 * Important functions (you will use these)
 *   importqueue_queue_add add a new queue item
 *   importqueue_queue_get retrieve the next matching queue item
 *
 * Private functions
 *   importqueue_queueplugin_get get an instance of the active queue backend handler
 *
 */

/**
 * Settings for for Queue Backend plugin type
 */
function importqueue_plugin_queue_settings_form(&$form, &$form_state, $settings, $log) {

  $info = importqueue_plugin_info('queue', NULL, $log);
  $plugin_options = array();
  foreach($info['plugins'] as $key=>$plugin) {
    $plugin_options[$key] = $plugin['label'].' ('.$key.')';
  }

  $form['active_backend'] = array(
    '#type' => 'radios',
    '#title' => t('Active queue backend'),
    '#description' => t('This selected backend will be used for queue item management. <strong>Changing backends will not copy existing queue items into the new backend, effectively losing all queue items.</strong>'),
    '#options' => $plugin_options,
    '#default_value' => isset($settings['active_backend']) ? $settings['active_backend'] : 'QueueFile'
  );

}

/**
 * Add a new queue item to the default queue
 *
 * @param String item type plugin handler for the item
 * @param String $id unique identifier for the Queue Item
 * @param Array $data data for the item
 * @param importqueue_Log logger
 *
 * @return boolean success
 */
function importqueue_queue_item_add($type, $id, importqueue_DataStruct $data, importqueue_Log &$log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $status = ImportQueue_Queue::STATUS_QUEUED;

  // convert the params to data and metadata
  $data->type = $type;
  $data->id = $id;
  $data->time = microtime();
  $data->date = date('Y-m-d');

  $queue = importqueue_queueplugin_get(array());
  $log->message('Creating new QUEUE item : '.$id, array('queue'=>$queue, 'data'=>$data), importqueue_Log::SEVERITY_MESSAGE);
  return $queue['class']::createItem($data, $status, $log);
}

/**
 * Retrieve the next matching queue item
 *
 * @param Array $filters
 * @returns a importqueue_Queue_Item item matching the parameters
 *    or NULL if no matching queue items are found.
 */
function importqueue_queue_item_get(Array $filters=NULL, importqueue_Log &$log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }
  if (!is_array($filters)) {
    $filters = array();
  }
  // a quick hack for min priority
  if (isset($filters['minPriority'])) {
    $filters['priority'] = array('op'=>'>=', 'value'=>$filters['minPriority']);
    unset($filters['minPriority']);
  }

  $queue = importqueue_queueplugin_get(array(), $log);
  $log->message('Retrieving item from QUEUE', array('queue'=>$queue, 'fiters'=>$filters),importqueue_Log::SEVERITY_MESSAGE);
  return $queue['class']::getItem($filters, $log);
}

/**
 * Get a static instance instance of the active queue plugin
 *
 * @requires plugin type settings to deteriming what plugin to load
 * @param Array @optional $options array which will be included in the plugin instance
 *    only on the first function call
 * @returns active queue plugin class instance
 */
function importqueue_queueplugin_get($options=array(), importqueue_Log &$log=NULL) {
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $plugin =& drupal_static(__function__, NULL);
  if (is_null($plugin)) {

    try {

      $settings = importqueue_plugintype_settings_get('queue', $log);
      $type = $settings['active_backend'];

      if (empty($type)) {
        throw new importqueue_QueueException_InvalidQueuePlugin('Undefined ImportQueue queue plugin.  The "active" queue plugin was not properly configured, and cannot be loaded.  Trying to load the first available queue plugin instead.');
      }
      $plugin = importqueue_plugin_info('queue', $type, $log);
      if (empty($plugin)) {
        throw new importqueue_QueueException_InvalidQueuePlugin('Invalid ImportQueue queue plugin.  The "active" queue plugin cannot be found.  Trying to load the first available queue plugin instead.');
      }

    } catch (Exception $e) {

      $info = importqueue_plugin_info('queue', $type, $log);
      $type = key($info['plugins']);
      $plugin = importqueue_plugin_info('queue', $type, $log);

      if (empty($plugin)) {
        throw new importqueue_QueueException_CannotInitializeQueue('No valid ImportQueue queue plugins were found.', array(), 0, $e);
      }

      $log->exception($e);
    }

  }

  return $plugin;
}


/**
 * @interface importqueue_Queue Backend Queue handler for Import Queue Queue
 *
 * Any queue handler has to be able to implement this interface in order for
 * the queue handling to be able to add items to it, and manage those items.
 */
interface ImportQueue_Queue {

  /**
   * Status codes for queue items
   */
  const STATUS_QUEUED = 'queued';
  const STATUS_CLAIMED = 'claimed';
  const STATUS_CLOSED = 'closed';
  const STATUS_FAILED = 'failed';

  /**
   * Add a new ItemData object to the queue
   *
   * @return boolean success
   */
  static public function createItem(importqueue_DataStruct $data, $status=importqueue_Queue_Item::STATUS_QUEUED, ImportQueue_Log $log);

  /**
   * Get the next queue item, that matches optional filter criteria
   *
   * @todo improve the filtering methodology, maybe allow multiple values?
   *
   * @return importqueue_Queue_Item instance  which can ->release() and ->close()
   *     Or empty/NULL if no queue items found
   */
  static public function getItem(Array $metaFilters=NULL, ImportQueue_Log $log);

  /**
   * List the queue items that match the passed filter, for admin use
   *
   * @todo improve the filtering methodology, maybe allow multiple values?
   *
   * @note this functionality was implemented before by pulling all
   *   of the items from the queue, and then releasing them.  This is
   *   a better way, that may allow us to check on items in other
   *   states
   *
   * @return an array of importqueue_DataStruct objects for queue items
   */
  static public function listItems(Array $metaFilters=NULL, ImportQueue_Log $log);

  /**
  * Retrieve the current status for the item
  *
  * @returns One of the status constants;
  */
  public function status();

  /* Queue operations for the queue item */

  /**
   * Mark the item as claimed, pulling it from the queue
   */
  public function claim();

  /**
   * Mark the item as open, put it back into the queue
   */
  public function release();

  /**
   * Mark the item as processed and closed in the queue
   */
  public function close();

  /**
   * Mark the item as processed and closed in the queue
   */
  public function fail();

  /**
   * Mark the item as processed and closed in the queue
   */
  public function remove();


  /**
   * Save the queue item, keeping any changes in the data struct
   */
  public function save();

  /**
   * Retrieve ItemData object for this item
   *
   * @returns a importqueue_DataStruct of meta() and data()
   */
  public function data();

  /**
   * Retrieve Queue specific information for this item
   *
   * @returns an array of queue information
   */
  public function info();

}

/**
 * Some common queue exceptions
 *
 */

/**
 * @exception importqueue_QueueException_CannotInitializeQueue Import Queue Queue init failed.  We cannot process any queue items
 *
 */
class importqueue_QueueException_CannotInitializeQueue extends importqueue_ExceptionCritical { }

/**
 * @exception importqueue_QueueException_CannotInitializeQueue Import Queue Queue init failed.  We cannot process any queue items
 *
 */
class importqueue_QueueException_InvalidQueuePlugin extends importqueue_ExceptionError { }

/**
 * @exception importqueue_QueueException_CannotProcessQueueItem Import Queue Queue item could not be found or was broken
 *
 */
class importqueue_QueueException_CannotProcessQueueItem extends importqueue_ExceptionError { }

/**
 * @exception importqueue_QueueException_NoMatchingItemFound Retrieving an item failed, as no matching item was found
 *
 * @extends importqueue_ExceptionMessage
 */
class importqueue_QueueException_NoMatchingItemFound extends importqueue_ExceptionMessage { }

/**
 * @exception importqueue_QueueException_ItemAlreadyExists A matching queue item already exists
 *
 * @extends importqueue_ExceptionMessage
 */
class importqueue_QueueException_ItemAlreadyExists extends importqueue_ExceptionMessage { }

/**
 * @exception importqueue_QueueException_QueueItemNotInterpretable A retrieved queue item does not make sense
 *
 * @extends importqueue_ExceptionMessage
 */
class importqueue_QueueException_QueueItemNotInterpretable  extends importqueue_ExceptionWarning { }

/**
 * @exception importqueue_QueueException_QueueItemHasNoData A retrieved queue item does not make sense
 *
 * @extends importqueue_ExceptionError
 */
class importqueue_QueueException_QueueItemHasNoData  extends importqueue_ExceptionError { }
