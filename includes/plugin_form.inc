<?php
/**
 * @file plugin_form.inc Plugin setttings and state form handlers and form submit handlers
 *
 * This functionality is split from the general plugin definitions and functions as it is only
 * used for certain admin functions and comprises a fair number of LOC
 *
 * @note Any code that wants to use these functions should run importqueue_loadincludes('plugin_form');
 */

/**
 * Form handler function for admin/settings for a plugin type
 *
 * @see FormAPI
 */
function importqueue_plugintype_settings_form($form, &$form_state, $type, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['#log'] = $log;

  $info = importqueue_plugin_info($type, NULL, $log);
  $settings = $info['settings'];

  if (!empty($info)) {

    /**
     * Generate a wrapper for the settings form
     */
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t($info['label'].' settings'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
      '#info' => $info,
    );

    if (isset($info['options_form'])) {
      /**
      * pass only the wrapper for the settings to the plugin
      */
      $function = $info['options_form'];
      $function($form['settings'], $form_state, $settings, $log);

      $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('save'),
        '#submit' => array('importqueue_plugintype_settings_form_submit'),
      );
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => t('reset'),
        '#submit' => array('importqueue_plugintype_settings_form_submit'),
      );
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('cancel'),
        '#submit' => array('importqueue_plugintype_settings_form_submit'),
      );
    }
    else {

    $form['settings']['nosettings'] = array(
      '#markup' => '<strong>This plugin type '.$type.' has no settings</strong>',
    );

    }
  }
  else {
    $form['badplugin'] = array(
      '#markup' => '<strong>Could not find any information for type '.$type.'</strong>',
    );
  }

  return $form;
}
function importqueue_plugintype_settings_form_submit($form, &$form_state) {
  if (isset($form['#log'])) {
    $log = $form['#log'];
  }
  else {
    $log = importqueue_log_get();
  }

  switch ($form_state['input']['op']) {
    case 'reset':
      $log->message('Resetting plugin type settings for '.$form_state['values']['type'],array('type'=>$form_state['values']['type']), ImportQueue_Log::SEVERITY_EVENT);
      importqueue_plugintype_settings_set($form_state['values']['type'], array(), $log);
      break;

    default:
    case 'save':
      importqueue_plugin_form_InterpetSafeArrays($form['settings'], $form_state['values']['settings']);
      if (isset($form_state['values']['settings'])) {
        $log->message('Saving plugin settings for '.$form_state['values']['type'],array('type'=>$form_state['values']['type'], 'settings'=>$form_state['values']['settings']), ImportQueue_Log::SEVERITY_EVENT);
        importqueue_plugintype_settings_set($form_state['values']['type'], (array)$form_state['values']['settings'], $log);
      }
      break;
  }
}

/**
 * Form handler function for state for a plugin type
 *
 * @see FormAPI
 */
function importqueue_plugintype_state_form($form, &$form_state, $type, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['#log'] = $log;

  $info = importqueue_plugin_info($type, NULL, $log);
  $state = importqueue_plugintype_state_get($type, $log);

  if (!empty($info)) {

    /**
     * Generate a wrapper for the state form
     */
    $form['state'] = array(
      '#type' => 'fieldset',
      '#title' => t($info['label'].' state'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
      '#info' => $info,
    );

    if (isset($info['state_form'])) {
      /**
      * pass only the wrapper for the state to the plugin
      */
      $function = $info['state_form'];
      $function($form['state'], $form_state, $state, $log);

      $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('save state'),
        '#submit' => array('importqueue_plugintype_state_form_submit'),
      );
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => t('reset state'),
        '#submit' => array('importqueue_plugintype_state_form_submit'),
      );
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('cancel'),
        '#submit' => array('importqueue_plugintype_state_form_submit'),
      );
    }
    else {

    $form['state']['nostate'] = array(
      '#markup' => '<strong>This plugin type '.$type.' has no state</strong>',
    );

    }
  }
  else {
    $form['badplugin'] = array(
      '#markup' => '<strong>Could not find any information for type '.$type.'</strong>',
    );
  }

  return $form;
}
function importqueue_plugintype_state_form_submit($form, &$form_state) {
  if (isset($form['#log'])) {
    $log = $form['#log'];
  }
  else {
    $log = importqueue_log_get();
  }

  switch ($form_state['input']['op']) {
    case 'reset state':
      $log->message('Resetting plugin type state for '.$form_state['values']['type'],array('type'=>$form_state['values']['type']), ImportQueue_Log::SEVERITY_EVENT);
      importqueue_plugintype_state_set($form_state['values']['type'], array(), $log);
      break;

    default:
    case 'save state':
      importqueue_plugin_form_InterpetSafeArrays($form['state'], $form_state['values']['state']);
      if (isset($form_state['values']['state'])) {
        $log->message('Saving plugin state for '.$form_state['values']['type'],array('type'=>$form_state['values']['type'], 'state'=>$form_state['values']['state']), ImportQueue_Log::SEVERITY_EVENT);
        importqueue_plugintype_state_set($form_state['values']['type'], (array)$form_state['values']['state'], $log);
      }
      break;
  }
}

/**
 * Form handler function for admin/settings for a plugin
 *
 * @see FormAPI
 */
function importqueue_plugin_settings_form($form, &$form_state, $type, $plugin, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['plugin'] = array(
    '#type' => 'value',
    '#value' => $plugin,
  );
  $form['#log'] = $log;

  $info = importqueue_plugin_info($type, $plugin, $log);
  $class = $info['class'];

  if (class_exists($class)) {

    /**
     * Generate a wrapper for the settings form
     */
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t($info['label'].' settings'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );
    /**
     * If we have a configuratble plugin, then pass the settings
     * value to $class::options_form()
     */
    if (is_a($class, 'ImportQueue_Configurable', TRUE)) {

      $class::options_form($form['settings'], $form_state, $log);
      importqueue_plugin_form_MakeSafeArrays($form['settings']);

      $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('save settings'),
        '#submit' => array('importqueue_plugin_settings_form_submit'),
      );
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => t('reset settings'),
        '#submit' => array('importqueue_plugin_settings_form_submit'),
      );
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('cancel'),
        '#submit' => array('importqueue_plugin_settings_form_submit'),
      );
    }
    else {
      $form['settings']['nosettings'] = array(
        '#markup' => '<strong>This plugin has no configurable settings</strong>',
      );
    }
  }
  else {
    $form['badplugin'] = array(
      '#markup' => '<strong>Could not find a proper plugin for '.$plugin.'</strong>',
    );
  }

  return $form;
}
/**
 * when submitting the above form, if save was clicked, pass the form settings to the setting save function
 */
function importqueue_plugin_settings_form_submit($form, &$form_state) {
  if (isset($form['#log'])) {
    $log = $form['#log'];
  }
  else {
    $log = importqueue_log_get();
  }

  switch ($form_state['input']['op']) {
    case 'reset settings':
      $log->message('Resetting plugin settings for '.$form_state['values']['plugin'],array('type'=>$form_state['values']['type'], 'plugin'=>$form_state['values']['plugin']), ImportQueue_Log::SEVERITY_EVENT);
      importqueue_plugin_settings_set($form_state['values']['type'], $form_state['values']['plugin'], array(), $log);
      break;

    default:
    case 'save settings':
      importqueue_plugin_form_InterpetSafeArrays($form['settings'], $form_state['values']['settings']);
      if (isset($form_state['values']['settings'])) {
        $log->message('Saving plugin settings for '.$form_state['values']['plugin'],array('type'=>$form_state['values']['type'], 'plugin'=>$form_state['values']['plugin'], 'settings'=>$form_state['values']['settings']), ImportQueue_Log::SEVERITY_EVENT);
        importqueue_plugin_settings_set($form_state['values']['type'], $form_state['values']['plugin'], (array)$form_state['values']['settings'], $log);
      }
      break;
  }
}

/**
 * Form handler function for state for a plugin
 *
 * @see FormAPI
 */
function importqueue_plugin_state_form($form, &$form_state, $type, $plugin, ImportQueue_Log $log=NULL) {
  importqueue_loadincludes();
  if (is_null($log)) {
    $log = importqueue_log_get();
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['plugin'] = array(
    '#type' => 'value',
    '#value' => $plugin,
  );
  $form['#log'] = $log;

  $info = importqueue_plugin_info($type, $plugin, $log);
  $class = $info['class'];

  if (class_exists($class)) {

    /**
     * Generate a wrapper for the state form
     */
    $form['state'] = array(
      '#type' => 'fieldset',
      '#title' => t($info['label'].' state'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );

    /**
     * If we have a statefull plugin, then pass the state
     * value to $class::state_form()
     */

    if (is_a($class, 'ImportQueue_Stateful', TRUE)) {
      $state = importqueue_plugin_state_get($type, $plugin, $log);
      $class::state_form($form['state'], $form_state, $state, $log);

      $form['state']['#state'] = $state;
      importqueue_plugin_form_MakeSafeArrays($form['state']);

      $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('save state'),
        '#submit' => array('importqueue_plugin_state_form_submit'),
      );
      $form['reset'] = array(
        '#type' => 'submit',
        '#value' => t('reset state'),
        '#submit' => array('importqueue_plugin_state_form_submit'),
      );
      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('cancel'),
        '#submit' => array('importqueue_plugin_state_form_submit'),
      );
    }
    else {
      $form['state']['nostate'] = array(
        '#markup' => '<strong>This plugin has no configurable state</strong>',
      );
    }
  }
  else {
    $form['badplugin'] = array(
      '#markup' => '<strong>Could not find a proper plugin for '.$plugin.'</strong>',
    );
  }

  return $form;
}
/**
 * when submitting the above form, if save was clicked, pass the form state to the state save function
 */
function importqueue_plugin_state_form_submit($form, &$form_state) {
  if (isset($form['#log'])) {
    $log = $form['#log'];
  }
  else {
    $log = importqueue_log_get();
  }

  // #running is only used for display really.
  unset($form_state['values']['running']);

  switch ($form_state['input']['op']) {
    case 'reset state':
      $log->message('Resetting plugin state for '.$form_state['values']['plugin'],array('type'=>$form_state['values']['type'], 'plugin'=>$form_state['values']['plugin']), ImportQueue_Log::SEVERITY_EVENT);
      importqueue_plugin_state_set($form_state['values']['type'], $form_state['values']['plugin'], array(), $log);
      break;

    default:
    case 'save state':
      if (isset($form_state['values']['state'])) {
        if (isset($form['state']['#state'])) {
          $form_state['values']['state'] = drupal_array_merge_deep($form['state']['#state'], $form_state['values']['state']);
        }

        importqueue_plugin_form_InterpetSafeArrays($form['state'], $form_state['values']['state']);
        $log->message('Saving plugin state for '.$form_state['values']['plugin'],array('type'=>$form_state['values']['type'], 'plugin'=>$form_state['values']['plugin'], 'state'=>$form_state['values']['state']), ImportQueue_Log::SEVERITY_EVENT);
        importqueue_plugin_state_set($form_state['values']['type'], $form_state['values']['plugin'], (array)$form_state['values']['state'], $log);
      }
      break;
  }
}

/**
 * SAFE ARRAYS
 *
 * Several form values may need to be simple 1-level arrays.  This allows you
 * to do that converting the arrays into a standard multi-row text value,
 * 1-row/element, keys|value separated.
 * To use them, add a #safeArray to an element.
 *
 * @TODO do this better
 */

// convert any #safeArray element from an array to a string
function importqueue_plugin_form_MakeSafeArrays(&$form_element) {
  foreach (element_children($form_element) as $key) {
    $child =& $form_element[$key];
    importqueue_plugin_form_MakeSafeArrays($child);
  }

  if (isset($form_element['#safeArray']) && $form_element['#safeArray'] && is_array($form_element['#default_value'])) {
    $form_element['#default_value'] = _importqueue_plugin_form_ToFormArray($form_element['#default_value']);
  }
}
// intepret and #safeArrays element by converting it's string value back to an array
function importqueue_plugin_form_InterpetSafeArrays($form_element, &$form_state_values) {

  foreach (element_children($form_element) as $key) {
    if (isset($form_state_values[$key]) && is_array($form_state_values[$key])) {
      $child =& $form_element[$key];
      $values =& $form_state_values[$key];
      importqueue_plugin_form_InterpetSafeArrays($child, $values);
    }
  }

  if (isset($form_element['#safeArray']) && $form_element['#safeArray']) {
    $value = _importqueue_plugin_form_FromFormArray($form_element['#value']);
    $form_element['#value'] = $value;
    $form_state_values = $value;
  }
}
// convert an array to a string
function _importqueue_plugin_form_ToFormArray(Array $values) {
  $safeValues = array();
  foreach($values as $key=>$value) {
    if (is_int($key)) {
      $safeValues[$key] = $value;
    }
    else {
      $safeValues[$key] = $key.'|'.$value;
    }
  }
  return implode("\n", $safeValues);
}
// convert a string back to an array
function _importqueue_plugin_form_FromFormArray($values) {
  $array = array();
  foreach(explode("\n", $values) as $value) {
    $value = explode('|', $value, 2);
    if (count($value)==0) {
      $array[] = $value[0];
    }
    else {
      $array[$value[0]] = $value[1];
    }
  }
  return $array;
}
